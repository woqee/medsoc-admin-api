// flow-typed signature: d9757ee150ff4b2008820a6b770c0dc7
// flow-typed version: <<STUB>>/dockerlint_v0.2.0/flow_v0.42.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'dockerlint'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'dockerlint' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'dockerlint/bin/dockerlint' {
  declare module.exports: any;
}

declare module 'dockerlint/lib/checks' {
  declare module.exports: any;
}

declare module 'dockerlint/lib/cli' {
  declare module.exports: any;
}

declare module 'dockerlint/lib/parser' {
  declare module.exports: any;
}

declare module 'dockerlint/lib/utils' {
  declare module.exports: any;
}

// Filename aliases
declare module 'dockerlint/bin/dockerlint.js' {
  declare module.exports: $Exports<'dockerlint/bin/dockerlint'>;
}
declare module 'dockerlint/lib/checks.js' {
  declare module.exports: $Exports<'dockerlint/lib/checks'>;
}
declare module 'dockerlint/lib/cli.js' {
  declare module.exports: $Exports<'dockerlint/lib/cli'>;
}
declare module 'dockerlint/lib/parser.js' {
  declare module.exports: $Exports<'dockerlint/lib/parser'>;
}
declare module 'dockerlint/lib/utils.js' {
  declare module.exports: $Exports<'dockerlint/lib/utils'>;
}
