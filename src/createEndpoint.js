/* @flow */

type Context = {
    request: any;
    response: any;
    next: Function;
}

type Result = {
    statusCode: number;
    body: Object | Array<Object>;
}

export default (spec: Object, handler: (context: Context) => Promise<Result> | Result): any => {
    const endpoint = (request, response, next) => {
        const respond = (responds) => {
            if(!responds) {
                return;
            }

            const {statusCode, body} = responds
            const validationError = response.validateResponse(statusCode, body);

            if (validationError) {
                next(validationError);
                return;
            }

            if(statusCode) {
                response.status(statusCode).json(body);
            }
        };

        const result = handler({request, response, next});

        Promise.resolve(result).then(respond).catch(next);
    };

    endpoint.apiDoc = spec;

    return endpoint;
};

export const formatResult = (statusCode: number, body: any): Result => ({
    statusCode,
    body,
});

export const formatSuccessfulResult = (body: Object): Result => formatResult(200, body);
