/* @flow */
import createEndPoint, {formatResult} from '../createEndpoint';
import getMembers from '../user/getMembers';

const requestParameters = [
    {
        in: 'query',
        name: 'societyCode',
        type: 'string',
        required: true,
    },
    {
        in: 'query',
        name: 'search',
        type: 'string',
        required: false,
    }
];

const responseSchema = {
    type: 'array',
    items: {
        type: 'object',
        properties: {
            _id: {
                type: 'object',
            },
            active: {
                type: 'boolean',
            },
            email: {
                type: 'string',
            },
            firstName: {
                type: 'string',
            },
            lastName: {
                type: 'string',
            },
            membershipStatus: {
                type: 'string',
            },
            society: {
                type: 'object',
            },
            specialization: {
                type: 'string',
            },
            membershipStatus: {
                type: 'string',
            },
            type: {
                type: 'string',
            },
            payments: {
                type: 'array',
            },
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const {societyCode, search} = request.query;

    return getMembers(societyCode, search).then((result) => {
        return formatResult(200, result);
    });
};

export const get = createEndPoint(spec, handler);
