/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import passwordRecovery from '../user/passwordrecovery';

const requestParameters = [
    {
        name: 'email',
        required: true,
        in: 'query',
        type: 'string',
    },
];

const responseSchema = {};
const spec = {
    parameters: requestParameters,
    responses: {
        '201': {
            description: 'Password Request Sent',
            schema: responseSchema,
        },
    },
};


const handler = ({request}) => {
    const {email} = request.query

    console.log(email)
    // return passwordRecovery(email)
    //     .then(() => formatResult(201, {}))
};

export const get = createEndPoint(spec, handler);
