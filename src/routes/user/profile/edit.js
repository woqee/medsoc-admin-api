/* @flow */
import createEndPoint, {formatSuccessfulResult} from '../../../createEndpoint';
import R from 'ramda';
import moment from 'moment';
import updateUserInfo from '../../../user/updateUserInfo'
import uploadProfileToS3, {getFileStream, createWriteStream} from '../../../user/uploadProfileToS3'
import sharp from 'sharp'

const requestParameters = [
    {
        name: 'profileImg',
        in: 'formData',
        type: 'file',
    },
    {
        name: 'body',
        in: 'body',
        schema: {
            type: 'object',
            properties: {
                firstName: {
                    type: 'string'
                },
                lastName: {
                    type: 'string'
                },
                specialization: {
                    type: 'string'
                }
            }
        }
    }
];

const responseSchema = {
    type: 'object',
    properties: {
        message: {
            type: 'string',
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const getFiles = async (request) => {
    if(!request.files) {
        return {}
    }

    const userName = `${request.user.firstName}-${request.user.lastName}`
    const {profileImg} = request.files;

    const path = profileImg.path ? profileImg.path : profileImg
    const outputFile = await sharp(path).jpeg({quality: 40}).toBuffer()

    const url = await uploadProfileToS3({
        fileStream: outputFile,
        originalFilename: `${userName}.png`,
    })
    return {
        profileImg: url,
    }
}

const handler = async ({request}) => {
    const {society, _id} = request.user;
    const { firstName, lastName, specialization } = request.body

    const profileImg = await getFiles(request)
    const properties = {
        ...profileImg,
        ...request.body
    }

    return updateUserInfo(_id, properties)
    .then(() => formatSuccessfulResult({
        updated: properties
    }))
};

export const post = createEndPoint(spec, handler);
