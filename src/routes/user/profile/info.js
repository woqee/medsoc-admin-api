/* @flow */
import createEndPoint, {formatResult} from '../../../createEndpoint';
import getUserFromRequest from 'basic-auth';
import mongoConnect from '../../../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../../../mongodb/mongoConstant';
import R from 'ramda'
const requestParameters = [

];

const responseSchema = {
    type: 'object',
    properties: {
        _id: {
            type: 'object',
        },
        email: {
            type: 'string',
        },
        firstName: {
            type: 'string',
        },
        lastName: {
            type: 'string',
        },
        logoURL: {
            type: 'string',
        },
        society: {
            type: 'string',
        },
        societyCode: {
            type: 'string',
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const {name} = getUserFromRequest(request);

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.users).findOne({
            email: name,
        });
    }).then((user) => {

        return formatResult(200, R.omit([
            'passwordRecoveryToken',
            'password',
            'registrationKey'
        ])(user));
    })
};

export const get = createEndPoint(spec, handler);
