/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import mailgun from '../../mailgun';
import createAdmin from '../../user/createAdmin';
import adminExists from '../../user/adminExists';
import shortid from 'shortid';

const requestParameters = [
    {
        name: 'firstName',
        required: true,
        in: 'formData',
        type: 'string'
    },
    {
        name: 'lastName',
        required: true,
        in: 'formData',
        type: 'string'
    },
    {
        name: 'email',
        required: true,
        in: 'formData',
        type: 'string'
    },
    {
        name: 'societyCode',
        required: true,
        in: 'formData',
        type: 'string',
        enum: ['PUA', 'AFN', 'PCS'],
    },
    {
        name: 'temporaryPassword',
        required: false,
        in: 'formData',
        description: 'minimum of 6 alpha numeric characters',
        type: 'string',
        minLength: 6,
        pattern: '^[a-zA-Z0-9]+$',
    },
];

const responseSchema = {};
const spec = {
    parameters: requestParameters,
    responses: {
        '201': {
            description: 'User Admin Invited',
            schema: responseSchema,
        },
    },
};


const handler = ({request}) => {
    const {email, temporaryPassword} = request.body

    const password = temporaryPassword ? temporaryPassword : shortid.generate()

    return adminExists(request.body)
    .then((existingUser) => {
        if(existingUser) {
            throw Error('Admin exists')
        }

        return createAdmin({
            ...request.body,
            password,
        })
    })
    .then(() => {
        return formatResult(201, {
            password: password,
            message: 'Successfully Added an admin'
        })
    })
    .catch((error) => {
        throw Error(error)
    })
};

export const post = createEndPoint(spec, handler);
