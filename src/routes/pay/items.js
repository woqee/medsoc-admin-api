/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import getAllDoctorSociety from '../../society/getAllDoctorSociety';
import getUnPaidItemsBySocietyCodes from '../../items/getUnPaidItemsBySocietyCodes';
import getUnpaidArrearsByUserId from '../../arrears/getUnpaidArrearsByUserId'
import {BANK_FEE, TRANSACTION_FEE} from '../../systemDefault';
import calculatePaymentTotal from '../../items/calculatePaymentTotal'

const requestParameters = [];

const responseSchema = {
    type: 'object',
    items: {
        type: 'object',
        properties: {
            bankFee: {
                type: 'number',
            },
            transactionFee: {
                type: 'number',
            },
            societies: {
                type: 'object',
                properties: {
                    _id: {
                        type: 'object',
                    },
                    societyName: {
                        type: 'string',
                    },
                    itemName: {
                        type: 'string',
                    },
                    societyCode: {
                        type: 'string',
                    },
                    isAbsorbBankingFees: {
                        type: 'boolean',
                    },
                    transactionFee: {
                        type: 'number',
                    },
                    price: {
                        type: 'number',
                    },
                },
            },
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const _id = request.userId;

    return getAllDoctorSociety(_id)
    .then((societies) => getUnPaidItemsBySocietyCodes(_id, societies))
    .then((items) => {

        return items.map((item) => {
            const {price} = item
            const isAbsorbBankingFees = item.isAbsorbBankingFees ? item.isAbsorbBankingFees : 'false'

            const paymentTotals = calculatePaymentTotal({
                currency: 'PHP',
                price,
                isAbsorbBankingFees
            })

            return {
                ...item,
                price,
                isAbsorbBankingFees,
                total: price
            }
        })

    })
    .then((result) => formatResult(200, {
        bankFee: BANK_FEE,
        transactionFee: TRANSACTION_FEE,
        items: result,
    }));
};

export const get = createEndPoint(spec, handler);
