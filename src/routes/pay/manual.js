/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import setManualPayment from '../../items/setManualPayment';
import removeManualPaymentInItem from '../../items/payment/removeManualPaymentInItem'
import getItemById from '../../items/getItemById';

const requestParameters = [
    {
        name: 'body',
        required: true,
        in: 'body',
        schema: {
            type: 'object',
            required: ['userId', 'itemId'],
            properties: {
                userId: {
                    type: 'string',
                },
                revert: {
                    type: 'boolean',
                },
                itemId: {
                    type: 'string',
                },
            }
        }
    },
];

const responseSchema = {
    type: 'object',
    items: {
        type: 'object',
        properties: {
            societies: {
                type: 'object',
                properties: {
                    _id: {
                        type: 'object',
                    },
                    name: {
                        type: 'string',
                    },
                    itemName: {
                        type: 'string',
                    },
                    societyCode: {
                        type: 'string',
                    },
                    total: {
                        type: 'number',
                    },
                },
            },
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '201': {
            description: 'Successful creation',
            schema: responseSchema,
        },
        '500': {
            description: 'Not Allowed',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const {
        userId,
        itemId,
        revert,
    } = request.body;
    const {type} = request.user;

    if(type !== 'admin') {
        return formatResult(500, {message: 'Not Allowed'});
    }

    return getItemById(itemId)
    .then((item) => {
        const payment = {
            itemId,
            userId,
            item,
            operatorId: request.userId,
        };

        if(revert) {
            return removeManualPaymentInItem(payment)
        }
        return setManualPayment(payment);
    })
    .then((result) => {
        return formatResult(201, {message: 'Success'});
    });
};

export const post = createEndPoint(spec, handler);
