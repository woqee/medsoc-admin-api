/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import getPaymentLog from '../../activityLogs/getPaymentLog';
import getPaidLogs from '../../reports/getPaidLogs';
import getRegistrationLogs from '../../reports/getRegistrationLogs';
import getSocietyById from '../../society/getSocietyById';
import R from 'ramda';
import moment from 'moment';

const requestParameters = [
    {
        name: 'type',
        in: 'query',
        required: true,
        type: 'string',
        enum: ['payment', 'registration'],
    }
];

const responseSchema = {
        type: 'object',
        properties: {
            type: {
                type: 'string',
            },
            logs: {
                type: 'array',
                items: {
                    type: 'object',
                    properties: {
                        id: {
                            type: 'string'
                        },
                        message: {
                            type: 'string',
                        },
                    }
                }
            },
        },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const paymentLogFormatter = (data) => data.map((result) => ({
    id: result.id,
    date: result.paidDate,
    message: `${moment(result.paidDate).format('MMM DD YYYY m:s A')}: ${result.payee.firstName} ${result.payee.lastName} paid P ${result.payment.amountToRemit} for the ${result.item.itemName}`,//'<Date & Time>: <Complete Name of Doctor> paid <amount> for <item name>'
}));

const handler = ({request}) => {
    const {society} = request.user;
    const {type} = request.query;

    if(type === 'payment') {
        return getSocietyById(society)
        .then(({societyCode}) => getPaidLogs(societyCode))
        .then((result) => {
            return formatResult(200, {
                type: 'payment',
                logs: paymentLogFormatter(result),
            });
        });
    } else {
        return getSocietyById(society)
        .then(({societyCode}) => getRegistrationLogs(societyCode))
        .then((result) => {
            return formatResult(200, {
                type: 'registration',
                logs: result,
            });
        });
    }


};

export const get = createEndPoint(spec, handler);
