/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import getNumberOfRegisteredUser from '../../reports/getNumberOfRegisteredUser';
import R from 'ramda';

const requestParameters = [
];

const responseSchema = {
        type: 'object',
        properties: {
            registered: {
                type: 'number',
            },
        },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const {society} = request.user;
    const {type} = request.query;


    return Promise.all([getNumberOfRegisteredUser()])
    .then((result) => {
        console.log(result)
        return formatResult(200, {
            registered: result[0],
        });
    })


};

export const get = createEndPoint(spec, handler);
