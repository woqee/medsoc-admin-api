/* @flow */
import createEndPoint, {formatResult} from '../createEndpoint';
import getSocieties from '../society/getSocieties';
import R from 'ramda'

const requestParameters = [

];

const responseSchema = {
    type: 'array',
    items: {
        type: 'object',
        properties: {
            _id: {
                type: 'object',
            },
            societyName: {
                type: 'string',
            },
            societyCode: {
                type: 'string',
            },
            logoURL: {
                type: 'string',
            },
            type: {
                type: 'string',
            }
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const pickPropResult = R.map((result) => ({
    ...result,
    groupCode: !result.groupCode ? 'n/a' : result.groupCode
}))

const handler = ({request}) => {
    const query = request.query
    return getSocieties(query)
    .then(pickPropResult)
    .then((result) => {
        return formatResult(200, result);
    });
};

export const get = createEndPoint(spec, handler);
