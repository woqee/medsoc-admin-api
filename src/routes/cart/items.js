/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import addNewItemCart from '../../cart/addNewItemCart'
import getCart from '../../cart/getCart'

const postSpec = {
    parameters: [{
        name: 'body',
        in: 'body',
        schema: {
            type: 'object',
            required: ['itemIds'],
            properties: {
                itemIds: {
                    type: 'array',
                    items: {
                        type: 'string'
                    }
                },
            },
        },
    }],
    responses: {
        '201': {
            description: 'Item added to cart',
            schema: {},
        },
    },
};

export const post = createEndPoint(postSpec, async ({request}) => {
    const userId = request.userId
    const itemIds = request.body.itemIds

    const cartId = await addNewItemCart({
        userId,
        itemIds
    })

    return formatResult(201, {
        result: {
            message: 'success',
            cartId
        },
    })
})

const getSpec = {
    parameters: [],
    responses: {
        '200': {
            description: 'Successful request',
            schema: {

            }
        },
    },
}

export const get = createEndPoint(getSpec, async ({request}) => {
    const userId = request.userId
    const cart = await getCart(userId)

    return formatResult(200, {
        cartId: cart._id,
        result: cart ? cart.itemIds : [],
    })
})
