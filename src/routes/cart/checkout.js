/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import addNewItemCart from '../../cart/addNewItemCart'
import getCartPayment from '../../cart/getPayment'
import getItemByIds from '../../items/getItemByIds'
import calculatePaymentTotal from '../../items/calculatePaymentTotal'
import R from 'ramda'
//
// const postSpec = {
//     parameters: [{
//         name: 'body',
//         in: 'body',
//         schema: {
//             type: 'object',
//             required: ['itemIds'],
//             properties: {
//                 itemIds: {
//                     type: 'array',
//                     items: {
//                         type: 'string'
//                     }
//                 },
//             },
//         },
//     }],
//     responses: {
//         '201': {
//             description: 'Item added to cart',
//             schema: {},
//         },
//     },
// };
//
// export const post = createEndPoint(postSpec, async ({request}) => {
//     const userId = request.userId
//     const itemIds = request.body.itemIds
//
//     const cartId = await addNewItemCart({
//         userId,
//         itemIds
//     })
//
//     return formatResult(201, {
//         result: {
//             message: 'success',
//             cartId
//         },
//     })
// })

const getSpec = {
    parameters: [],
    responses: {
        '200': {
            description: 'Successful request',
            schema: {

            }
        },
    },
}

export const get = createEndPoint(getSpec, async ({request}) => {
    const userId = request.userId
    const result = await getCartPayment(userId)

    return formatResult(200, {
        result
    })
})
