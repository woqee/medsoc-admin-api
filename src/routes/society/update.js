/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import uploadLogoToS3, {getFileStream, createWriteStream} from '../../society/uploadLogoToS3'
import sharp from 'sharp'
import updateSociety from '../../society/updateSociety'
import R from 'ramda'
const requestParameters = [

];

import createAdmin from '../../user/createAdmin'

const responseSchema = {
    type: 'object',
    properties: {},
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const getFiles = async (request) => {
    if(!request.files || R.isEmpty(request.files)) {
        return {}
    }

    const userName = `${request.user.firstName}-${request.user.lastName}`
    const {logoURL} = request.files;

    const path = logoURL.path ? logoURL.path : logoURL
    // const outputFile = await sharp(/path).jpeg({quality: 40}).toBuffer()

    const url = await uploadLogoToS3({
        fileStream: getFileStream(path),
        originalFilename: logoURL.originalFilename,
    })

    return {
        logoURL: url,
    }
}

const handler = async ({request}) => {
    const logoURL = await getFiles(request)

    const { body } = request

    const id = body.id
    const withoutID = R.omit('id')(body)
    const updateBody = {
        ...logoURL,
        ...withoutID
    }

    await updateSociety(id, updateBody)

    return formatResult(200, {
        message: 'Succcess'
    })
};

export const post = createEndPoint(spec, handler);
