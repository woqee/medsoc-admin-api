/* @flow */
import createEndPoint, {formatResult} from '../createEndpoint';
import getUserFromRequest from 'basic-auth';
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import R from 'ramda'

const requestParameters = [

];

const responseSchema = {
    type: 'object',
    properties: {
        _id: {
            type: 'object',
        },
        email: {
            type: 'string',
        },
        firstName: {
            type: 'string',
        },
        lastName: {
            type: 'string',
        },
        logoURL: {
            type: 'string',
        },
        society: {
            type: 'string',
        },
        societyCode: {
            type: 'string',
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const {name} = getUserFromRequest(request);
    const countryCode = request.headers["cf-ipcountry"]

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.users).find({
            email: name,
        }).toArray();
    }).then((model) => {
        const user = model[0];

        if(user.type === 'superadmin') {
            return formatResult(200, {...user});
        }

        if(user.type === 'doctor') {
            return formatResult(200, {...user});
        }

        return mongoConnect.then((db) => {
            return db.collection(COLLECTION.societies).find(ObjectID(user.society)).toArray();
        }).then((result) => {
            if (R.isEmpty(result)) {
                return user
            }

            return {
                ...user,
                society: user.society,
                societyName: result[0].societyName,
                logoURL: result[0].logoURL,
                societyCode: result[0].societyCode,
                countryCode: 'PH',
            }
        }).then((result) => {
            return formatResult(200, result);
        });
    })
};

export const get = createEndPoint(spec, handler);
