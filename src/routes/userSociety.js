/* @flow */
import createEndPoint, {formatResult} from '../createEndpoint';
import getDoctorSociety from '../society/getDoctorSociety';
import getSocietiesByCode from '../society/getSocietiesByCode';

const requestParameters = [
];

const responseSchema = {
    type: 'object',
    items: {
        type: 'object',
        properties: {
            societies: {
                type: 'array',
                items: {
                    type: 'object',
                    properties: {
                        _id: {
                            type: 'object',
                        },
                        societyName: {
                            type: 'string',
                        },
                        societyCode: {
                            type: 'string',
                        },
                        logoURL: {
                            type: 'string',
                        },
                        type: {
                            type: 'string',
                        },
                    },
                },
            },
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const _id = request.userId;

    return getDoctorSociety(_id)
        .then(getSocietiesByCode)
        .then((societies) => {
            return formatResult(200, {societies});
        }).catch((error) => {
            return formatResult(200, {societies: []});
        });
};

export const get = createEndPoint(spec, handler);
