/* @flow */
import createEndPoint, {formatResult} from '../createEndpoint';
import createItem from '../items/create/createItem';

const requestParameters = [
    {
        name: 'body',
        required: true,
        in: 'body',
        schema: {
            type: 'object',
            properties: {
                token: {
                    type: 'string',
                },
                items: {
                    type: 'array',
                },
            },
        },
    },
];

const responseSchema = {
    type: 'object',
};

const spec = {
    parameters: requestParameters,
    responses: {
        '201': {
            description: 'Successful request',
            schema: responseSchema,
        },
        '301': {
            description: 'Not created',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const query = request.body;

    return createItem(query).
    then((result) => {
        return formatResult(201, {result: 'OK'});
    });
};

export const post = createEndPoint(spec, handler);
