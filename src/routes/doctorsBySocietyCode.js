/* @flow */
import createEndPoint, {formatResult} from '../createEndpoint';
import getDoctorsBySocietyCode from '../user/getDoctorsBySocietyCode';

const requestParameters = [
    {
        in: 'query',
        name: 'societyCode',
        type: 'string',
        required: true,
    },
];

const responseSchema = {
    type: 'object',
    items: {
        type: 'object',
        properties: {
            societies: {
                type: 'array',
                items: {
                    type: 'object',
                    properties: {
                        _id: {
                            type: 'object',
                        },
                        societyName: {
                            type: 'string',
                        },
                        societyCode: {
                            type: 'string',
                        },
                        logoURL: {
                            type: 'string',
                        },
                        type: {
                            type: 'string',
                        },
                    },
                },
            },
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
        '500': {
            description: 'Error in request',
        },
    },
};

const handler = ({request}) => {
    const {societyCode} = request;

    return getDoctorsBySocietyCode(societyCode)
        .then((doctors) => {
            return formatResult(200, {doctors});
        });
};

export const get = createEndPoint(spec, handler);
