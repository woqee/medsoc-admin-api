/* @flow */

import createEndPoint, {formatResult} from '../createEndpoint';
import createArrears from '../arrears/create'
import createLog from '../activityLogs/createLog'
import deleteArrear from '../arrears/deleteArrear'
import getArrearsByUserId from '../arrears/getArrearsByUserId'

const postSpec = {
    parameters: [
        {
            name: 'body',
            required: true,
            in: 'body',
            schema: {
                type: 'object',
                required: [
                    'societyCode',
                ],
                properties: {
                    societyCode: {
                        type: 'string',
                    },
                    arrears: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: {
                                userId: {
                                    type: 'string',
                                },
                                arrearName: {
                                    type: 'string',
                                },
                                price: {
                                    type: 'number',
                                },
                            },
                        }
                    },
                },
            },
        },
    ],
    responses: {
        '201': {
            description: 'Successful request',
            schema: {
                type: 'object',
            },
        },
        '301': {
            description: 'Not created',
            schema: {
                type: 'object',
            },
        },
    },
};

const deleteSpec = {
    parameters: [
        {
            name: 'body',
            required: true,
            in: 'body',
            schema: {
                type: 'object',
                properties: {
                    id: {
                        type: 'string',
                    },
                },
            },
        }
    ],
    responses: {
        '202': {
            description: 'Arrear deleted',
            schema: {
                type: 'object',
            },
        },
        '301': {
            description: 'Not allowed',
            schema: {
                type: 'object',
            },
        },
    },
}

const getSpec = {
    parameters: [
        {
            name: 'societyCode',
            required: true,
            in: 'query',
            type: 'string'
        }
    ],
    responses: {
        '200': {
            description: 'Successful',
            schema: {
                type: 'array',
            },
        },
        '301': {
            description: 'Not allowed',
            schema: {
                type: 'object',
            },
        },
    },
}


export const post = createEndPoint(postSpec, ({request}) => {
    const {
        societyCode,
        arrears,
    } = request.body
    const adminId = request.userId;

    if(!societyCode) {
        throw new Error('SocietyCode not available')
    }

    return createArrears(arrears, societyCode)
    .then(async () => {
        arrears.map(async ({arrearName}) => await createLog({
            logName: 'created arrear',
            arrearName,
            adminId,
        }, societyCode))

        return formatResult(201, {result: 'success'})
    })
});


export const get = createEndPoint(getSpec, async ({request}) => {
    const {userId, societyCode} = request.query
    const result = await getArrearsByUserId(userId, societyCode)

    return formatResult(200, result)
})

export const del = createEndPoint(deleteSpec, ({request}) => {
    const {id} = request.body

    return deleteArrear(id)
    .then(({deletedCount}) => {
        if(deletedCount) {
            return formatResult(202, {result: 'deleted'})
        }

        return formatResult(301, {result: 'not allowed'})
    })
})
