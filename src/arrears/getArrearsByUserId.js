/* @flow */

import {aggregate} from '../mongodb'
import {COLLECTION} from '../mongodb/mongoConstant'
import {ObjectID} from 'mongodb'
import R from 'ramda'

const aggregateArrears = aggregate(COLLECTION.items)

type Result = {
    userId: string,
    arrearName: string,
    cost: number,
    createdAt: Object,
    paid: boolean,
    societyCode: string,
}

const formatResult = R.map((result) => ({
    ...result,
    paid: R.not(R.isEmpty(result.paid)),
}))

export default (userId: string, societyCode: string): Promise<[Result]> => {
    const aggregation = [
        {
            $match: {
                userId: ObjectID(userId),
                societyCode,
            },
        },
        {
             $lookup:{
                 from: "payments",
                 localField: "_id",
                 foreignField: "itemId",
                 as: "paid",
             },
        },
        {
            $project: {
                _id: 1,
                paid: 1,
                userId: 1,
                arrearName: '$itemName',
                price: 1,
                createdAt: 1,
                societyCode: 1,
            },
        }
    ]

    return aggregateArrears(aggregation).then(formatResult)
}
