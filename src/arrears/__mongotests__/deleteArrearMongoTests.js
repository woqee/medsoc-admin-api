/* @flow */

import deleteArrear from '../deleteArrear'
import {COLLECTION} from '../../mongodb/mongoConstant'
import prepareMongoTest from '../../mongodb/prepareMongoTest'
import {ObjectID} from 'mongodb'

const arrearsData = [
    {
        _id: ObjectID('58d67efe05cf345b05964a81'),
        userId: '2',
        arrearName: 'arrear 1',
        cost: 200,
        createdAt: new Date('2017-03-01'),
        paid: false,
    },
    {
        _id: ObjectID('58d67efe05cf345b05964a82'),
        userId: '2',
        arrearName: 'arrear 2',
        cost: 150,
        createdAt: new Date('2017-03-01'),
        paid: true,
    },
]

describe('delete arrear', () => {
    it('deletes an arrear', () => {
        return prepareMongoTest(COLLECTION.arrears, arrearsData, async () => {
            const {deletedCount} = await deleteArrear('58d67efe05cf345b05964a81')

            expect(deletedCount).toEqual(1)
        })
    })

    it('should return an error if deleting a paid arrear', () => {
        const paidID = '58d67efe05cf345b05964a82'

        return prepareMongoTest(COLLECTION.arrears, arrearsData, async () => {
            deleteArrear(paidID)
            .catch((e) => (
                expect(e).toThrow()
            ))
        })
    })
})
