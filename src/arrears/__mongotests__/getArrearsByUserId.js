/* @flow */

import getArrearsByUserId from '../getArrearsByUserId'
import prepareMongoTest from '../../mongodb/prepareMongoTest'
import {COLLECTION} from '../../mongodb/mongoConstant'

const arrearsData = [
    {
        userId: '58d67efe05cf345b05964a81',
        arrearName: 'arrear 1',
        cost: 200,
        createdAt: new Date('2017-03-01'),
        paid: false,
    },
    {
        userId: '58d67efe05cf345b05964a82',
        arrearName: 'arrear 2',
        cost: 150,
        createdAt: new Date('2017-03-01'),
        paid: true,
    },
    {
        userId: '58d67efe05cf345b05964a82',
        arrearName: 'arrear 3',
        cost: 150,
        createdAt: new Date('2017-03-01'),
        paid: false,
    },
    {
        userId: '58d67efe05cf345b05964a82',
        arrearName: 'arrear 4',
        cost: 150,
        createdAt: new Date('2017-03-01'),
        paid: false,
    },
]

describe('Get arrears by user Id', () => {
    it('returns the arrears of the specified Id', () => {
        return prepareMongoTest(COLLECTION.arrears, arrearsData, async () => {
            const result = await getArrearsByUserId('58d67efe05cf345b05964a82')

            expect(result).toEqual([
                {
                    userId: '58d67efe05cf345b05964a82',
                    arrearName: 'arrear 2',
                    cost: 150,
                    createdAt: new Date('2017-03-01'),
                    paid: true,
                },
                {
                    userId: '58d67efe05cf345b05964a82',
                    arrearName: 'arrear 3',
                    cost: 150,
                    createdAt: new Date('2017-03-01'),
                    paid: false,
                },
                {
                    userId: '58d67efe05cf345b05964a82',
                    arrearName: 'arrear 4',
                    cost: 150,
                    createdAt: new Date('2017-03-01'),
                    paid: false,
                },
            ])
        })
    })

    it('returns the unpaid arrears only of specified id', () => {
        return prepareMongoTest(COLLECTION.arrears, arrearsData, async () => {
            const result = await getArrearsByUserId('58d67efe05cf345b05964a82', {
                paid: false
            })

            expect(result).toEqual([
                {
                    userId: '58d67efe05cf345b05964a82',
                    arrearName: 'arrear 3',
                    cost: 150,
                    createdAt: new Date('2017-03-01'),
                    paid: false,
                },
                {
                    userId: '58d67efe05cf345b05964a82',
                    arrearName: 'arrear 4',
                    cost: 150,
                    createdAt: new Date('2017-03-01'),
                    paid: false,
                },
            ])
        })
    })
})
