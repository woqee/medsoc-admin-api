import createArrears from '../create'

describe('Create Mongo test', () => {
    it('creates a arrears', async () => {
        const arrears = [
            {
                userId: '1',
                itemName: 'test',
                price: 12,
                societyCode: 'PUA',
            },
        ]

        return prepareMongoTest(COLLECTION.carts, [], async () => {
            const result = await createArrears(arrears, 'societyCode')

            expect(result.insertedCount).toEqual(1)
        })



    })
})
