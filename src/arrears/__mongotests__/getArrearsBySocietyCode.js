/* @flow */

import getArrearsBySocietyCode from '../getArrearsBySocietyCode'
import prepareMongoTest from '../../mongodb/prepareMongoTest'
import {COLLECTION} from '../../mongodb/mongoConstant'

const arrearsData = [
    {
        userId: '58d67efe05cf345b05964a81',
        arrearName: 'arrear 1',
        cost: 200,
        createdAt: new Date('2017-03-01'),
        paid: false,
        societyCode: 'PUA',
    },
    {
        userId: '58d67efe05cf345b05964a82',
        arrearName: 'arrear 2',
        cost: 150,
        createdAt: new Date('2017-03-01'),
        paid: true,
        societyCode: 'PUA',
    },
    {
        userId: '58d67efe05cf345b05964a82',
        arrearName: 'arrear 3',
        cost: 150,
        createdAt: new Date('2017-03-01'),
        paid: false,
        societyCode: 'PUA',
    },
    {
        userId: '58d67efe05cf345b05964a82',
        arrearName: 'arrear 4',
        cost: 150,
        createdAt: new Date('2017-03-01'),
        paid: false,
        societyCode: 'PALES',
    },
]

describe('getArrearsBySocietyCode', () => {
    it('get all by given societyCode', () => {
        const societyCode = 'PUA'

        return prepareMongoTest(COLLECTION.arrears, arrearsData, async () => {
            const result = await getArrearsBySocietyCode(societyCode)

            expect(result).toEqual([
                {
                    userId: '58d67efe05cf345b05964a81',
                    arrearName: 'arrear 1',
                    cost: 200,
                    createdAt: new Date('2017-03-01'),
                    paid: false,
                    societyCode: 'PUA',
                },
                {
                    userId: '58d67efe05cf345b05964a82',
                    arrearName: 'arrear 2',
                    cost: 150,
                    createdAt: new Date('2017-03-01'),
                    paid: true,
                    societyCode: 'PUA',
                },
                {
                    userId: '58d67efe05cf345b05964a82',
                    arrearName: 'arrear 3',
                    cost: 150,
                    createdAt: new Date('2017-03-01'),
                    paid: false,
                    societyCode: 'PUA',
                },
            ])
        })
    })

    it('returns an error if no societyCode given', () => {
        return prepareMongoTest(COLLECTION.arrears, arrearsData, async () => {
            getArrearsBySocietyCode()
            .catch((e) => (
                expect(e).toThrow()
            ))
        })
    })
})
