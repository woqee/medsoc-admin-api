/* @flow */

import mailgun from '../mailgun'

type EmailTemplate = {
    firstName: string,
    lastName: string,
    itemName: string,
    societyName: string,
    email: string,
}

const template = ({firstName, lastName, itemName, societyName, email}: EmailTemplate) => `<div style="color: gray; font-weight: 200;font-family:arial;font-size: 13px;margin: 60px">
 <div style="margin-bottom: 20px;padding-bottom: 10px;border-bottom: 1px solid gray">
  <img
       style="height: 40px"
       src='https://s17.postimg.org/iqbgj1jr3/Med_Soc_Logo.png'/>
 </div>
Dear Dr. ${firstName} ${lastName}
 <br/>
 <br/>
 An arrear from ${societyName} has been for on the Med Societies App. The app is available
 for download from the <a href="https://itunes.apple.com/ph/app/med-societies/id1122588574?mt=8"> App Store</a> or the <a href="https://play.google.com/store/apps/details?id=com.medsociety" >Play Store</a>.
 <br/>
 <br/>
 Warmest Regards,
 <br/>
 Med-Societies Team
 <br/>
 <br/>
 <br/>
 <span style='font-family:"open sans","source sans pro",verdana,tahoma,arial;font-size:10px;color:rgb(67,67,67);background-color:rgb(251,251,251);text-align:center;display:block'>
   <strong style='text-align:center;'>ABOUT THIS EMAIL: </strong><br/>
   You are receiving this message under the email address ${email} as a service from Med-Societies.
<br/>
Copyright © 2016 Woqee.com, Inc. | 2/F Amina Cuilding, Tandang Sora Ave., 1116 Quezon City, Philippines.

 </span>
</div>`

export default (query: EmailTemplate) => {
    const emailTemplate = {
        from: 'Med-Societies <support@med-societies.com',
        to: query.email,
        subject: 'New Item',
        html: template(query)
    }

    mailgun(emailTemplate, query.email)
}
