/* @flow */

import {aggregate} from '../mongodb'
import {COLLECTION} from '../mongodb/mongoConstant'
import {ObjectID} from 'mongodb'

const aggregateArrears = aggregate(COLLECTION.items)

type Result = {
    _id: Object,
    itemName: string,
    price: number,
    createdAt: Object,
    isAbsorbBankingFees: boolean,
    paidAt: Object,
    societyCode: string,
}

type itemIds = [{
    _id: string,
}]

export default (userId: string, itemIds: itemIds): Promise<[Result]> => {
    const aggregation = [
        {
            $match: {
                $or: itemIds.map(({_id}) => ({
                    _id: ObjectID(_id),
                })),
            },
        },
        {
            $project: {
                _id: 1,
                itemName: 1,
                name: '$itemName',
                societyCode: 1,
                isArrear: { $ifNull: [ "$type", 'arrear' ] },
                currency: { $ifNull: [ "$currency", 'PHP' ] },
                isAbsorbBankingFees: { $ifNull: [ "$isAbsorbBankingFees", 'false' ] },
                price: '$price',
            },
        }
    ]

    return aggregateArrears(aggregation)
}
