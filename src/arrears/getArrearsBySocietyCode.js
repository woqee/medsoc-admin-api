/* @flow */

import {aggregate} from '../mongodb'
import {COLLECTION} from '../mongodb/mongoConstant'

const aggregateArrears = aggregate(COLLECTION.items)

type Result = {
    userId: string,
    arrearName: string,
    cost: number,
    createdAt: Object,
    paid: boolean,
    paidAt: Object,
    societyCode: string,
}

export default (societyCode: string): Promise<[Result]> => {
    const aggregation = [
        {
            $match: {
                societyCode,
            },
        },
        {
            $project: {
                _id: 0,
                userId: 1,
                arrearName: '$itemName',
                price: 1,
                createdAt: 1,
                societyCode: 1,
            },
        }
    ]

    return aggregateArrears(aggregation)
}
