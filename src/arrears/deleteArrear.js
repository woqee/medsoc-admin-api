/* @flow */

import {COLLECTION} from '../mongodb/mongoConstant'
import {findOne, deleteOne} from '../mongodb'
import {ObjectID} from 'mongodb'

const findOneArrear = findOne(COLLECTION.items)
const deleteOneArrear = deleteOne(COLLECTION.items)

export default async (id: string): Promise<*> => {
    const arrear = await findOneArrear(id)

    if(!arrear) {
        throw new Error('id not found')
    }

    if(arrear.paid) {
        throw new Error('Cannot delete because it is already paid')
    }

    return deleteOneArrear(id)
}
