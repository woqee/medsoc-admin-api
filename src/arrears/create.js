/* @flow */

import {insert, update} from '../mongodb'
import {COLLECTION} from '../mongodb/mongoConstant'
import R from 'ramda'
import {ObjectID} from 'mongodb'
import getUserById from '../user/getUserById'
import getSocietyByCode from '../society/getSocietyByCode'
import sendMail from './emailTemplate'

const insertArrear = insert(COLLECTION.items)
const updateArrear = update(COLLECTION.items)

type Query = {
    itemId: string,
    societyCode: string,
    userId: string,
    arrearName: string,
    price: number,
}

export default async (queries: Array<Query>, societyCode: string) => {
    console.log(queries);
    const {firstName, lastName, email} = await getUserById(R.head(queries).userId)
    const { societyName } = await getSocietyByCode(societyCode)

    await sendMail({
        firstName,
        lastName,
        email,
        itemName: 'New arrear',
        societyName,
    })

    return queries.map(async ({userId, price, arrearName, itemId}) => {
        if(itemId) {
             await updateArrear({
                 _id: new ObjectID(itemId),
                 societyCode,
             }, {
                 itemName: arrearName,
                 updatedAt: new Date(),
                 price: Number(price),
             })

             return 'ok'
        }

        await insertArrear({
            userId: ObjectID(userId),
            name: societyName,
            itemName: arrearName,
            price: Number(price),
            societyCode,
            type: 'arrear',
            createdAt: new Date(),
        })

        return 'ok'
    })
}
