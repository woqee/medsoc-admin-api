/* @flow */

import {aggregate} from '../mongodb'
import {COLLECTION} from '../mongodb/mongoConstant'
import {ObjectID} from 'mongodb'

const aggregateArrears = aggregate(COLLECTION.items)

type Result = {
    userId: string,
    arrearName: string,
    cost: number,
    createdAt: Object,
    paid: boolean,
    paidAt: Object,
    societyCode: string,
}

export default (userId: string, matcher): Promise<[Result]> => {
    const matchers = matcher ? matcher : {}

    const aggregation = [
        {
            $match: {
                userId,
                ...matchers,
            },
        },
        {
             $lookup:{
                 from: "payments",
                 localField: "_id",
                 foreignField: "itemId",
                 as: "paidItems",
             },
        },
        {$match: {
              $nor: [{'paidItems.userId': new ObjectID(userId) }],
        }},
        {
             $lookup:{
                 from: "societies",
                 localField: "societyCode",
                 foreignField: "societyCode",
                 as: "society",
             },
        },
        {
            $project: {
                _id: 1,
                itemName: 1,
                name: { $arrayElemAt: ['$society.societyName', 0]},
                societyCode: 1,
                isArrear: { $ifNull: [ "$isArrear", true ] },
                currency: { $ifNull: [ "$currency", 'PHP' ] },
                isAbsorbBankingFees: { $ifNull: [ "$isAbsorbBankingFees", 'false' ] },
                price: '$price',
            },
        }
    ]


    return aggregateArrears(aggregation)
}
