/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (userId) => {
    const aggragation = [
        {$match: {
            userId: new ObjectID(userId),
        }}
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.userSociety)
            .aggregate(aggragation).toArray();
    });
}
