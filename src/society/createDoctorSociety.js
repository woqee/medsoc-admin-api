/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (userId, society) => {
    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.userSociety)
            .insert({
                userId: ObjectID(userId),
                societyId: ObjectID(society._id),
                societyCode: society.societyCode,
                status: society.status,
                active: true,
                createdAt: new Date(),
            });
    });
}
