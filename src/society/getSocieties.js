/* @flow */

import {COLLECTION} from '../mongodb/mongoConstant';
import {aggregate} from '../mongodb'
import {ObjectID} from 'mongodb'

const aggregateSocieties = aggregate(COLLECTION.societies)

export default (query) => {
    const matchQuery = query.id ? [{
            $match: {
                _id: ObjectID(query.id)
            }
        }] : []

    const aggregation = [
        ...matchQuery,
        {
            $sort: {
                groupCode: -1,
            },
        },
    ]

    return aggregateSocieties(aggregation)
}
