/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import R from 'ramda'

export default (userId: string, societyCode: string) => {
    if(!userId) {
        return Promise.resolve({})
    }

    const aggregation = [
        {
            $match: {
                userId: new ObjectID(userId),
                societyCode,
            }
        },
        {
            $lookup: {}
        }
    ]

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.userSociety).aggregate(aggregation)
    })
}
