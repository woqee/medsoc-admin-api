/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (_id, societies) => {
    try {
        societies.map((society) => {
            mongoConnect.then((db) => {
                return db.collection(COLLECTION.userSociety)
                .update({
                    userId: ObjectID(_id),
                    societyCode: society.societyCode,
                }, {
                    $set: {
                        userId: ObjectID(_id),
                        societyId: ObjectID(society._id),
                        societyCode: society.societyCode,
                        active: true,
                        lastUpdate: new Date(),
                    }
                }, {
                    upsert: true,
                });
            });
        });
        return Promise.resolve('success');
    } catch(error) {
        return Promise.reject(error);
    }
}
