/* @flow */

import fs from 'fs'
import uploadFile from '../aws/s3/uploadFile'

const MED_SOC_BUCKET = 'societies'

const getUnixtime = () => new Date().getTime()

export const getFileStream = (path: string) => fs.createReadStream(path)
export const createWriteStream = (path: string) => fs.createWriteStream(path)

const getImageUrl = (bucket: string, uploadPath: string) =>
    `https://s3.ap-southeast-1.amazonaws.com/${bucket}/${uploadPath}`

export default async ({fileStream, originalFilename}): Promise<string> => {
    const uploadPath = `logos/${originalFilename || ''}`

    const file = {
        body: fileStream,
        name: uploadPath,
    }

    await uploadFile(MED_SOC_BUCKET, file)

    return getImageUrl(MED_SOC_BUCKET, uploadPath)
}
