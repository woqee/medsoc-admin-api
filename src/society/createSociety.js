import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default async (society) => {
    const db = await mongoConnect

    const result = await db.collection(COLLECTION.societies).findOne({
        societyCode: society.societyCode,
    });

    if(result) {
        throw new Error('society has been added')
    }

    return db.collection(COLLECTION.societies)
        .insert({
            societyCode: society.societyCode,
            societyName: society.societyName,
            groupCode: society.groupCode,
            groupLead: society.groupLead,
            email: society.email,
            type: society.type || 'specialty',
            logoURL: society.logoURL,
            createdAt: new Date(),
        });
}
