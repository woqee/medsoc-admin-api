/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (societies) => {
    const societyCodes = societies.map(({societyCode}) => {
        return {societyCode};
    });

    const aggregation = [
        {$match: {
            $or: societyCodes,
        }}
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.societies).aggregate(aggregation).toArray();
    });
}
