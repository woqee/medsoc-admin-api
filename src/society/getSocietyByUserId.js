/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import R from 'ramda'

export default (userId: string, societyCode: string, user) => {
    if(!userId) {
        return Promise.resolve({})
    }

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.userSociety).findOne({
            userId: new ObjectID(userId),
            societyCode,
        })
    }).then((userSociety) => {
        return {
            ...userSociety,
            userId,
            registrationKey: user.registrationKey,
        }
    })
}
