import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default async (id, society) => {
    const db = await mongoConnect

    return db.collection(COLLECTION.societies)
        .update(
            {
                _id: ObjectID(id)
            },
            {
                $set: society
            }
        );
}
