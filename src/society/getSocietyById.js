/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (societyId) => {
    return mongoConnect.then((db) =>
        db.collection(COLLECTION.societies).findOne({
            _id: ObjectID(societyId)
        })
    )
}
