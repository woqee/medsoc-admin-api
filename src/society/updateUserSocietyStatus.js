/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (_id, societyCode, status) => {
    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.userSociety)
        .update({
            userId: ObjectID(_id),
            societyCode: societyCode,
        }, {
            $set: {
                status,
            }
        });
    });
}
