/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (userId) => {
    const aggregation = [
        {$match: {
            userId: new ObjectID(userId),
            active: true,
        }}
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.userSociety)
            .aggregate(aggregation).toArray();
    });
}
