/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import passwordGenerate from '../authentication/passwordGenerate';

type society = {
    _id: string,
    societyName: string,
    societyCode: string,
    logoURL: string,
    type: string,
}

export default async (societyCode: string): Promise<society> => {
    const db = await mongoConnect

    return db.collection(COLLECTION.societies).findOne({
        societyCode,
    });
}
