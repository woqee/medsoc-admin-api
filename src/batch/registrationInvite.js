/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import getWebUrlByEnvironment from '../getWebUrlByEnvironment'
import mailgun from '../mailgun';

export default () => {
    return mongoConnect.then((db) =>
        db.collection(COLLECTION.users).find({type: 'doctor', active: false}).toArray()
    )
    .then((users) => {
        const url = getWebUrlByEnvironment('/register')

        users.map((result) => {
            const data = {
              from: 'Philippine Urological Association <pua@med-societies.com>',
              to: result.email,
              subject: 'Med Society App Admin Invitation',
              html: `
              <br/>
              Dear Dr. ${result.firstName},
              <br/>
              <br/>
              It is with great pleasure that the Philippine Urological Association introduce the newly launched online payment system for all association-related dues: the Med Societies Mobile App.
              <br/>
              <br/>
              In order to use this new system, simply go to this unique <a href="${url}?key=${result.registrationKey}">link</a>
            and complete your registration by filling out your profile information required, then download the app on your iPhone from the App Store or on your Android phone from the Play Store.
              <br/>
              <br/>
              If you can’t use the link above, please copy the url below and paste it on your browser.
              <br/>
              <a href="${url}?key=${result.registrationKey}">${url}?key=${result.registrationKey}</a>
              <br/>
              <br/>
              <br/>
              Constantino T. Castillo III
              <br/>
              Chairman of the Membership Committee
              <br/>
              Philippine Urological Association
              <br/>
              <br/>
              <br/>
              Pedro L. Lantin, III
              <br/>
              President
              <br/>
              Philippine Urological Association
              <br/>
              <br/>

              <div style='font-size:12.8px;'>-------------------------------------------------------------------------------</div>
              <br/>
               <div style='font-size:12.8px;'>This is a system-generated email. To reply to this or if you have questions, please email pua_org@yahoo.com</div>
              `,
            };

            mailgun(data, result.email);
        })
    })
}
