/* @flow */

import createCart from '../createCart'
import getCart from '../getCart'
import prepareMongoTest from '../../mongodb/prepareMongoTest'
import {COLLECTION} from '../../mongodb/mongoConstant'

describe('Create cart test', () => {
    it('creates a new cart', async () => {
        const info = [
            {
                userId: '1',
                cartName: 'new cart'
            },
        ]

        return prepareMongoTest(COLLECTION.carts, [], async () => {
            const result = await createCart(info)

            expect(result.insertedCount).toEqual(1)

            const cart = await getCart('1')

            expect(result.userId).toEqual('1')
        })
    })
})
