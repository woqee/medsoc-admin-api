/* @flow */

import {COLLECTION, update} from '../mongodb'
import getCart from './getCart'
import createCart from './createCart'
import R from 'ramda'

const updateCart = update(COLLECTION.carts)

type Query = {
    itemIds: Array<string>,
    userId: string
}

const mergeList = R.compose(
    R.uniq,
    R.concat
)

export default async (id): Promise<string> => {
    const cart = await getCart(userId)

    if(cart) {
        const result = await updateCart({
            _id: cart._id
        }, {
            itemIds
        })

        return Promise.resolve(cart._id)
    }

    return createCart({
        userId,
        itemIds
    })
}
