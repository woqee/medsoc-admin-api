/* @flow */

import {COLLECTION, findOne} from '../mongodb'
import {ObjectID} from 'mongodb'

const findOneCart = findOne(COLLECTION.carts)

export default (id: string): Promise<*> => {
    return findOneCart({
        _id: ObjectID(id),
        paid: false
    })
}
