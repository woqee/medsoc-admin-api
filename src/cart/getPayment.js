
import R from 'ramda'
import getCart from './getCart'
import getItemByIds from '../items/getItemByIds'
import calculatePaymentTotal from '../items/calculatePaymentTotal'

const getTotalPrice = (items) => {
    return items.reduce((acc, item) => {
        return R.add(acc, item.price)
    }, 0)
}

const getTotalPriceOverall = (items) => {
    return items.reduce((acc, item) => {
        return R.add(acc, item.priceOverall)
    }, 0)
}

export const getTotalItemsPriceWithFees = (items) => {
    const totals = items.map(({_id, currency, itemName, societyName, description, logoURL, price, isAbsorbBankingFees}) => {
        const paymentTotals = calculatePaymentTotal({
            currency: 'PHP',
            price,
            isAbsorbBankingFees
        })
        return {
            _id,
            itemName,
            currency: currency || 'PHP',
            societyName,
            logoURL,
            origPrice: price,
            description,
            ...paymentTotals,
            priceOverall: paymentTotals.price,
            price
        }
    })

    return totals
}

export const getTotalByProp = (prop) => R.compose(
    Number,
    (num) => num.toFixed(2),
    R.sum,
    R.map(R.prop(prop))
)

export default async (userId) => {
    const cart = await getCart(userId)

    const itemIds = cart.itemIds.map((id) => ({_id: id}))
    const items = await getItemByIds(itemIds)

    const totalPriceWithoutFees = getTotalPrice(items)
    const totalPriceWithFees = getTotalItemsPriceWithFees(items)
    const totalPrice = getTotalPrice(totalPriceWithFees)
    const totalPriceOverall = getTotalPriceOverall(totalPriceWithFees)

    const totalTransactionFees = getTotalByProp('totalTransactionFee')(totalPriceWithFees)
    const amountToTransfer = getTotalByProp('amountToTransfer')(totalPriceWithFees)

    return {
        _id: cart._id,
        items: totalPriceWithFees,
        totalTransactionFees,
        totalPriceWithoutFees,
        amountToTransfer,
        totalPrice,
        totalPriceOverall
    }
}
