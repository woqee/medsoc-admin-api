/* @flow */

import {COLLECTION, insert} from '../mongodb'
import {ObjectID} from 'mongodb'

const insertCart = insert(COLLECTION.carts)

type Body = {
    userId: string,
    itemIds: Array<string>
}

export default ({userId, itemIds}: Body): Promise<*> => {
    return insertCart({
        userId: ObjectID(userId),
        createdAt: new Date(),
        paid: false,
        itemIds
    }).then((result: any) => result.ops[0])
}
