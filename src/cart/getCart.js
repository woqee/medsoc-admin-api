/* @flow */

import {COLLECTION, findOne} from '../mongodb'
import {ObjectID} from 'mongodb'

const findOneCart = findOne(COLLECTION.carts)

export default (userId: string): Promise<*> => {
    return findOneCart({
        userId: ObjectID(userId),
        paid: false
    })
}
