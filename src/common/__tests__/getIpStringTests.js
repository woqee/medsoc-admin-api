import getIpString from '../getIpString';

describe('extracts ip', () => {
    it('returns the ipaddress from string', () => {
        const ip = "::ffff:112.198.69.242";

        expect(getIpString(ip)).toEqual('112.198.69.242');
    })
    it('returns the ipaddress from string width different case', () => {
        const ip = "::f:112.198.69.242";

        expect(getIpString(ip)).toEqual('112.198.69.242');
    })
})
