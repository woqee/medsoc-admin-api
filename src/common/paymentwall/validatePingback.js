/* @flow */
import crypto from 'crypto';

export default (params, ip, paymentwallkey) => {
    const ipsWhitelist = [
        '174.36.92.186',
        '174.36.96.66',
        '174.36.92.187',
        '174.36.92.192',
        '174.37.14.28'
    ];

    if(!~ipsWhitelist.indexOf(ip)) return false; // IP address not whitelisted

    // Check params
    let baseString = "", sig = params.sig;
    Object.keys(params).sort().forEach(function(key,i) {
        if(key === "sig") return;

        let value = params[key] || "";

        baseString += key + '=' + value;
    });
    baseString += paymentwallkey;

    var hash = crypto.createHash("md5").update(baseString).digest("hex");

    if(hash !== sig) return false; // Signature mismatch

    return true;
};
