/* @flow */

export default (ip) => {
    const regex='((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d]).*?';	// IPv4 IP Address 1
    const m = new RegExp(regex).exec(ip);

    if (m != null) {
        return m[1].replace(/</,"")
    }

    return null;
}
