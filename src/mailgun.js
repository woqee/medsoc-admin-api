/* @flow */
import mailgunAPI from 'mailgun-js';
import mailcomposer from 'mailcomposer';

const api_key = 'key-5e6fd16895abd826ce853bb285103f3b';
const domain = 'med-societies.com';

const mailgun = mailgunAPI({apiKey: api_key, domain: domain});

export default (data: string, to: string): Promise => new Promise((resolve, reject) => {

    const mail = mailcomposer(data);

    mail.build((mailBuildError, message) => {

        const dataToSend = {
            to,
            message: message.toString('ascii'),
        }

        mailgun.messages().sendMime(dataToSend, function (error, body) {
            if (error) {
                console.log(error);
                return reject(error);
            }
            console.log(`message sent to  ${to}`);

            return resolve(body);
        });
    })

});
