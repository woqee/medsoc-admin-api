/* @flow */


const PHP_RATE = 50
const DOLLAR_RATE = 1

export const TRANSACTION_FEE = 50;

export const getTransactionFee = (currency = 'PHP') => currency === 'PHP' ? PHP_RATE : DOLLAR_RATE
export const BANK_FEE = 0.045;
