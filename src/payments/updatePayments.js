
import {COLLECTION, update, insertMany} from '../mongodb'
import getCartById from '../cart/getCartById'
import createCart from '../cart/createCart'
import R from 'ramda'
import {ObjectID} from 'mongodb'
import getItemWithSocietyInfo from '../items/getItemWithSocietyInfo'
import societyEmailReport from './emailTemplate/societyEmailReport'
import getUserById from '../user/getUserById'
import customerEmailConfirmation from './emailTemplate/customerEmailConfirmation'

const updateCart = update(COLLECTION.carts)
const insertPayments = insertMany(COLLECTION.payments)

const updateCartPayload = (id, data) => updateCart({
    _id: ObjectID(id),
    paid: false
}, data)

export default async (body: any): Promise<*> => {
    const {
        status,
        receiptNumber,
        requestReferenceNumber,
        items,
        paymentType,
        totalAmount,
        paymentDetails,
        buyer,
        metadata,
        paymentStatus,
        id
    } = body
    const result = await getCartById(requestReferenceNumber)

    const { itemIds, userId} = result

    await updateCartPayload(requestReferenceNumber, {
        status,
        receiptNumber,
        paid: true,
        totalAmount,
        paidDate: new Date()
    })

    const payments = itemIds.map((itemId, key) => ({
        itemId: ObjectID(itemId),
        userId,
        paymentType,
        paidDate: new Date(),
        receiptNumber,
        metadata,
        item: items[key],
        otherPaymentInfo: {
            paymentId: id,
            buyer,
            paymentDetails,
            paymentStatus
        },
    }))

    await insertPayments(payments)

    //send email confirmation
    itemIds.map((itemId) => {
        Promise.all([
            getUserById(String(userId)),
            getItemWithSocietyInfo(itemId)
        ]).then(([user, item]) => {
            const email = item.society ? item.society.email : ''
            const societyName = item.society ? item.society.societyName : ''

            societyEmailReport({itemName: item.itemName, price: item.price, email, societyName, firstName: user.firstName, lastName: user.lastName })
        })
    })

    const user = await getUserById(String(userId))


    customerEmailConfirmation(items, totalAmount.value, {
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName
    })

    return
}
