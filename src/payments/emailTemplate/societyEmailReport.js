/* @flow */

import mailgun from '../../mailgun'

type EmailTemplate = {
    firstName: string,
    lastName: string,
    itemName: string,
    societyName: string,
    email: string,
}

const template = ({firstName, price, lastName, itemName, societyName, email}: EmailTemplate) => `
<div style="color: gray; font-weight: 200;font-family:arial;font-size: 13px;margin: 60px">
<div style="margin-bottom: 20px;padding-bottom: 10px;border-bottom: 1px solid gray">
<img
   style="height: 40px"
   src='https://s3-ap-southeast-1.amazonaws.com/societies/logos/Med_Soc_Logo.png'/>
</div>

<br/>
The ${itemName} from ${societyName} has been paid by ${firstName} ${lastName}

<br/>
<br/>
<b>Total Amount: </b> <br>
<b>PHP ${price}
<br/>
<div style="text-align: center;font-size: 11px">
this is a auto generated email
</div>
<br/>
<br/>
<br/>
<span style='font-family:"open sans","source sans pro",verdana,tahoma,arial;font-size:10px;color:rgb(67,67,67);background-color:rgb(251,251,251);text-align:center;display:block'>
<strong style='text-align:center;'>ABOUT THIS EMAIL: </strong><br/>
You are receiving this message under the email address ${email} as a service from Med-Societies.
<br/>
Copyright © 2018 Woqee.com, Inc. | 2/F Amina Cuilding, Tandang Sora Ave., 1116 Quezon City, Philippines.

</span>
</div>
`

export default (query: EmailTemplate) => {
    const emailTemplate = {
        from: 'Med-Societies <support@med-societies.com',
        to: query.email,
        subject: 'Customer Payment Confirmation',
        html: template(query)
    }

    mailgun(emailTemplate, query.email)

    return
}
