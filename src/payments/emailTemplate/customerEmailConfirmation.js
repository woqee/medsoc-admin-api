/* @flow */
import mailgun from '../../mailgun';
import priceFormat from '../../priceFormat'

export default (items, totalPayment, {email, firstName, lastName}) => {
    let payments = '';

    items.map((result) => {

        payments += `PHP ${(priceFormat(result.totalAmount.value))}: ${(result.description)} <br>`;
    });

    const emailTemplate = {
        from: 'Med Societies <support@med-societies.com>',
        to: email,
        subject: 'Medsoc Payment Receipt',
        html: `
        <pre>Dear ${firstName} ${lastName}, M.D.

        This is your receipt of payment against your credit card in the
        amount of PHP ${priceFormat(totalPayment)}

        Thank you.

        Payment Date: ${new Date()}
        Payment Details:

        ${payments}


        If you have any questions or concerns, please open a support ticket.

        Sincerely,

        The Woqee Team </pre>
        `};

    mailgun(emailTemplate, email);
}
