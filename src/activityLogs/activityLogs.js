/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import moment from 'moment';
import getUserById from '../user/getUserById'
import getSocietyById from '../society/getSocietyById'
import insertUserLogs from './insertUserLogs'

export const ADD_MEMBERS = 'addMembers';
export const MEMBER_JOINED = 'memberJoined';
export const PAID_SUCCESS = 'paidSuccess';
export const PAYMENT = 'payment';
export const MANUAL_PAYMENT = 'manualPayment';


const LogByPayment = (userId, body) => {
    return getUserById(userId)
    .then((user) => {
        const date = moment().format('MMM DD YYYY m:s A');
        const message = `${date}: ${user.firstName} ${user.lastName} paid P ${body.totalCost} for the ${body.itemName}`;

        const logs = {
            type: 'Payment',
            societyCode: body.societyCode,
            message,
            createdAt: new Date(),
            logDetail: body,
        }

        return insertUserLogs(logs)
    })
}

const LogAddMemberEvent = (userId, operator) => {
    return getUserById(userId)
    .then((user) => {
        const date = moment().format('MMM DD YYYY m:s A');
        const message = `${date}: ${operator} invited ${user.firstName} (${user.email}) as ${user.membershipStatus}`

        return getSocietyById(operator.society)
        .then(({societyCode}) => {
            const logs = {
                societyCode,
                message,
                createdAt: new Date(),
                logDetail: {
                    invited: user,
                },
            }
        })
    })
}

const LogMemberJoined = () => {

}

export const createLog = ({
    userId,
    type,
    body,
    operator,
}) => {
    const LOGS_QUERY = {
        [PAYMENT]: LogByPayment(userId, body),
        [ADD_MEMBERS]: LogAddMemberEvent(userId, operator),
        [MEMBER_JOINED]: LogAddMemberEvent(userId, operator),
    }

    return LOGS_QUERY[type]
}

const getMessageLog = ({user, society, date, operator, type}) => {
    const messages = Object.freeze({
        [ADD_MEMBERS]: `${date}: ${operator} invited ${user.firstName} (${user.email}) as ${user.membershipStatus}`,
        [MEMBER_JOINED]: `${date}: ${user.firstName} ${user.lastName} has joined ${society} (${user.email}) as ${user.membershipStatus}`,
    });

    return messages[type];
}

export default (type, user, body) => {
    const messageLog = {
        user,
        date: new Date(),
        type,
        operator,
    };

    mongoConnect.then((db) => {
        return db.collection(COLLECTION.logs).insert({
            operator: body.operatorId,
            logBody: {
                ...get
            },
        });
    })
}
