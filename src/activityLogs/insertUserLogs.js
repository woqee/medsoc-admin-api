/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import moment from 'moment'

export default (log) => {
    const date = moment().format('MMM DD YYYY m:s A');

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.logs).insert({
            ...log,
            logDate: new Date(),
        });
    })
}
