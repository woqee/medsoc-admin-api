/* @flow */

import createLog from '../createLog'

describe('Create Mongo test', () => {
    it('creates a logs', async () => {
        const arrears = {
            test: 1,
        }

        const societyCode = 'PUA'

        const result = await createLog(arrears, societyCode)

        expect(result.insertedCount).toEqual(1)
    })
})
