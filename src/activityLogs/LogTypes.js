
export default {
    PAYMENT: 'payment',
    MANUAL_PAYMENT: 'manual_payment',
    REVERT_MANUAL_PAYMENT: 'revert_manual_payment',
    CHECKOUT: 'checkout',
    ADDED_MEMBER: 'added_member',
}
