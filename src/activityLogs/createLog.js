/* @flow */

import {insert} from '../mongodb'
import {COLLECTION} from '../mongodb/mongoConstant'

const createLog = insert(COLLECTION.logs)

export default (queryPayload, societyCode) => {
    const payload = queryPayload ? queryPayload : null

    const query = {
        societyCode,
        timestamp: new Date(),
        payload,
    }

    return createLog(query)
}
