/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (societyCode) => {
    return mongoConnect.then((db) =>
        db.collection(COLLECTION.logs).find({
            'information.item.societyCode': societyCode,
        }).toArray()
    )
}
