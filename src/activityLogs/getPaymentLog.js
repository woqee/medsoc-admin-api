/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (societyCode) => {
    const aggregation = [
        {
            $match: {
              type: 'payment',
              'information.item.societyCode': societyCode,
            },
        },
        {
           $lookup:{
               from: "users",
               localField: "information.userId",
               foreignField: "_id",
               as: "payee",
           },
        },
        {
            $unwind: '$payee'
        },
    ];

    return mongoConnect.then((db) =>
        db.collection(COLLECTION.logs).aggregate(aggregation).toArray()
    )
}
