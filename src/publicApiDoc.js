/* @flow*/

export const apiDoc = {
    swagger: '2.0',
    basePath: '/v1/public',
    info: {
        title: 'Medsoc public API',
        version: '1.0.0',
    },
    definitions: {
        Error: {
            additionalProperties: true,
        },
    },
    paths: {},
    securityDefinitions: {
    },
    security: [
    ],
}
