/* @flow */

import numeral from 'numeral';

export default (price: number): string => {
    return numeral(price).format('0,0.00');
}
