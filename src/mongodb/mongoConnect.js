/* @flow */

import {MongoClient} from 'mongodb';

const MONGO_URL_LOCAL = 'mongodb://localhost:27017/medsoc';
const MONGO_URL_PROD = 'mongodb://medsocuser:woq239eee2@188.166.212.59:27017/medsoc';
const MONGO_URL_STAGING = 'mongodb://188.166.212.59:27017/medsoc_dev';
const MONGO_URL_DEMO = 'mongodb://medsocuser:woq239eee2@188.166.212.59:27017/medsoc_demo';

let MONGO_URL = process.env.MONGO_URL;

if(process.env.NODE_ENV === 'demo') {
    MONGO_URL = MONGO_URL_DEMO
}

export default MongoClient.connect(MONGO_URL);
