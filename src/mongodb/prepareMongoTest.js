/* @flow */

import mongoConnect from './mongoConnect'
import R from 'ramda'

export default R.curry(
    (collection: string, testData: mixed, callback: Function): Promise<*> =>
        mongoConnect.then((db: Object): Promise<*> =>
            db.collection(collection)
              .deleteMany({})
              .then((): Promise<*> => db.collection(collection).insertMany(testData))
              .then(callback),
          ),
)
