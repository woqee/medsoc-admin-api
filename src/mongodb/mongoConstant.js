/* @flow */

export const COLLECTION = Object.freeze({
    users: 'users',
    societies: 'societies',
    userSociety: 'userSociety',
    logs: 'logs',
    transaction: 'transaction',
    items: 'items',
    payments: 'payments',
    files: 'files',
    arrears: 'arrears',
    carts: 'carts',
    hooks: 'hooks'
});
