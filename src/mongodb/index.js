/* @flow */
import mongoConnect from './mongoConnect';
import {COLLECTION} from './mongoConstant';
import R from 'ramda'
import {ObjectID} from 'mongodb'

type Data = Object
type Aggregation = Object
type Collection = string

export {COLLECTION as COLLECTION} from './mongoConstant'

const isString = R.is(String)

export const insert =  R.curry((collection: Collection, data: Data) => mongoConnect.then((db) =>
    db.collection(COLLECTION[collection]).insert(data)
))

export const update =  R.curry((collection: Collection, find: Data, set: Data) => mongoConnect.then((db) =>
    db.collection(COLLECTION[collection]).update(find, {$set: set})
))

type DeleteQuery = Object | string

export const deleteOne =  R.curry((collection: Collection, query: DeleteQuery) => mongoConnect.then((db) =>
    typeof query === 'string' ?
        db.collection(COLLECTION[collection]).deleteOne({
            _id: new ObjectID(query),
        }) :
        db.collection(COLLECTION[collection]).deleteOne(query)

))

export const insertMany =  R.curry((collection: Collection, data: Array<Data>) => mongoConnect.then((db) =>
    db.collection(COLLECTION[collection]).insertMany(data)
))

type FindOneQuery = Object | string

export const findOne =  R.curry((collection: Collection, query: FindOneQuery) => mongoConnect.then((db) =>
    R.ifElse(
        isString,
        () => db.collection(COLLECTION[collection]).findOne({
            _id: new ObjectID(query),
        }),
        () => db.collection(COLLECTION[collection]).findOne(query),
    )(query)
))

export const aggregate = R.curry((collection: Collection, aggregation: Aggregation) => mongoConnect.then((db) =>
    db.collection(COLLECTION[collection]).aggregate(aggregation).toArray()
))

export const find = R.curry((collection: Collection, query: FindOneQuery) => mongoConnect.then((db) =>
    db.collection(COLLECTION[collection]).find(query).toArray()
))
