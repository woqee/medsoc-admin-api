/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import getItems from '../getItems';

const requestParameters = [
    {
        in: 'query',
        name: 'societyCode',
        type: 'string',
        required: true,
    }
];

const responseSchema = {
    type: 'object',
    items: {
        type: 'object',
        properties: {
            societies: {
                type: 'object',
                properties: {
                    _id: {
                        type: 'object',
                    },
                    name: {
                        type: 'string',
                    },
                    itemName: {
                        type: 'string',
                    },
                    required: {
                        type: 'string',
                    },
                    accessibleBy: {
                        type: 'string',
                    },
                    price: {
                        type: 'number',
                    },
                    isAbsorbBankingFees: {
                        type: 'string',
                    },
                    isEvent: {
                        type: 'string',
                    },
                    adminId: {
                        type: 'string',
                    },
                    societyCode: {
                        type: 'string',
                    },
                },
            },
        },
    },
};

export const getSpec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

export const getHandler = ({request}) => {
    const {societyCode} = request.query;

    return getItems(societyCode)
    .then((result) => {
        return formatResult(200, result);
    });
};

export const get = createEndPoint(spec, handler);
