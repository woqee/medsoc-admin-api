import calculatePaymentTotal from '../calculatePaymentTotal';

describe('Test calculation of payment', () => {
    const systemFee = 50;
    const bankFee = 0.044;

    it('calcalculates payment with absorbed', () => {
        const item = {
            price: 1500,
            isAbsorbBankingFees: 'true',
        };

        expect(calculatePaymentTotal(item)).toEqual({
            transactionFee: systemFee,
            totalCost: 1550,
            price: 1550,
            amountToRemit: 1434,
            totalTransactionFee: 50
        });
    });

    it('calculates payment with no absorbed', () => {
        const item = {
            price: 1500,
            isAbsorbBankingFees: 'false',
        };

        expect(calculatePaymentTotal(item)).toEqual({
            transactionFee: systemFee,
            bankFee,
            totalCost: 1621.34,
            amountToRemit: 1500,
            price: 1621.34,
            totalTransactionFee: 121.34
        });
    });
})
