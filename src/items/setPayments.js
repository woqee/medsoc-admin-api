/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import calculatePaymentTotal from './calculatePaymentTotal';

import httpAgent from '../httpAgent/httpAgent';
import createLog from '../activityLogs/insertUserLogs';
import logType from '../activityLogs/LogTypes';
import setPaymentEmail from './sendPaymentEmail'
import getUserById from '../user/getUserById'

export default ({
    items,
    userId,
    chargeInfo,
    bulkPaymentId,
}) => {
    const payments = items.map((item) => {
        const {_id} = item;
        const paymentDetail = calculatePaymentTotal(item);

        const log = {
            type: logType.PAYMENT,
            itemId: _id,
            triggeredBy: userId,
            logDate: new Date(),
            information: {
                userId,
                payment: paymentDetail,
                item,
            },
        };

        createLog(log);
        return {
            paymentType: logType.PAYMENT,
            paidDate: new Date(),
            userId: new ObjectID(userId),
            itemId: new ObjectID(_id),
            bulkPaymentId,
            item: item,
            payment: paymentDetail,
            chargeInfo,
        }
    });

    getUserById(userId).then((user) => {
        setPaymentEmail(items, {email: user.email, firstName: user.firstName, lastName: user.lastName})
    })

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.payments)
        .insertMany(payments);
    });
};
