/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import {ObjectID} from 'mongodb';

export default (userId, societyCode: string) => {
    const aggregation = [
        {$match: {societyCode: societyCode}},
        {
            $lookup:{
                from: "payments",
                localField: "_id",
                foreignField: "itemId",
                as: "paidItems",
            },
        },
        {$match: {
            'paidItems.userId': new ObjectId(userId),
        }},
        {$project: {
            id: '$_id',
            itemName: 1,
            name: 1,
            societyCode: 1,
            total: '$price',
        }}
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.items).aggregate(aggregation).toArray();
    });
}
