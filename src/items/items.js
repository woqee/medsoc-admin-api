/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (item) => {
    const {
        name,
        description,
        itemName,
        required = 'no',
        accessibleBy,
        price = 0,
        currency,
        isAbsorbBankingFees = false,
        isEvent = false,
        adminId,
        societyCode,
    } = item;

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.items).insert({
            name,
            itemName,
            description,
            required,
            accessibleBy,
            currency,
            price: parseFloat(price),
            isAbsorbBankingFees,
            isEvent,
            adminId,
            societyCode,
        });
    });
}
