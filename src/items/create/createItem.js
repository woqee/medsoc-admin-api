/* @flow */

import {insert} from '../../mongodb';
import {COLLECTION} from '../../mongodb/mongoConstant'
import getDoctorsBySocietyCode from '../../user/getDoctorsBySocietyCode'
import getSocietyByCode from '../../society/getSocietyByCode'
import sendMail from './emailTemplate'

const insertItems = insert(COLLECTION.items)

const formatter = ({
    name,
    description,
    itemName,
    required = 'no',
    accessibleBy,
    price = 0,
    isForeign = false,
    isAbsorbBankingFees = false,
    isEvent = false,
    adminId,
    societyCode,
}) => ({
    name,
    itemName,
    description,
    required,
    accessibleBy,
    isForeign,
    price: parseFloat(price),
    isAbsorbBankingFees,
    isEvent,
    adminId,
    societyCode,
})

export default async (item) => {
    const {societyName} = await getSocietyByCode(item.societyCode)
    const formattedItem = formatter({
        ...item,
        name: societyName,
    })

    return insertItems(formattedItem).then(async () => {
        const doctors = await getDoctorsBySocietyCode(item.societyCode, {
            status: item.accessibleBy === 'nonMember' ? 'pending': item.accessibleBy,
        })

        doctors.forEach(({firstName, lastName, email}) => {
            sendMail({
                firstName,
                lastName,
                email,
                itemName: item.itemName,
                societyName,
            })
        })
    })
}
