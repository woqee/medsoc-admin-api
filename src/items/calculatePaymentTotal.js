/* @flow */

import {BANK_FEE, getTransactionFee} from '../systemDefault';

const REMAINDER = 0.956

export default ({currency, price, isAbsorbBankingFees}) => {
    const TRANSACTION_FEE = getTransactionFee(currency)

    if(isAbsorbBankingFees === 'true') {
        const totalCost = (price + TRANSACTION_FEE)
        const payment = {
            transactionFee: TRANSACTION_FEE,
            totalCost,
            price: totalCost,
            totalTransactionFee: TRANSACTION_FEE,
            amountToRemit: (price - ( price * BANK_FEE )),
            amountToTransfer: (price - ( price * BANK_FEE )) + TRANSACTION_FEE,
        };

        return payment;
    }

    const totalCost = ((price + TRANSACTION_FEE) / REMAINDER)

    const totalCostConverted = Number(totalCost.toFixed(2))

    const totalTransactionFee = totalCostConverted - price
    const payment = {
        transactionFee: TRANSACTION_FEE,
        bankFee: BANK_FEE,
        totalCost: totalCostConverted,
        price: totalCostConverted,
        totalTransactionFee: Number(totalTransactionFee.toFixed(2)),
        amountToTransfer: price + TRANSACTION_FEE,
        amountToRemit: price,
    };

    return payment;
}
