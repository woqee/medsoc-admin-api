/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (societyCode: string) => {
    const aggregation = [
        {$match: {
            societyCode: societyCode,
            archive: {
                $exists: false,
            },
            $or: [
                { required: 'true'},
                { isEvent: 'true'},
            ]
        }},
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.items).aggregate(aggregation).toArray();
    });
}
