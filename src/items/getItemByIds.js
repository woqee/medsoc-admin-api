/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import {ObjectID} from 'mongodb';
import R from 'ramda'

const formatResult = R.map((result) => ({
    ...result,
    societyName: result.society[0].societyName,
    logoURL: result.society[0].logoURL,
    currency: result.currency ? result.currency : 'PHP',
}))

export default (itemIds: Array<*>) => {
    const mappedIds = itemIds.map(({_id}) => ({
        _id: ObjectID(_id),
    }));

    if (R.isEmpty(mappedIds)) {
        return []
    }
    const aggregation = [
        {
            $match: {
                $or: mappedIds,
            }
        },
        {
            $lookup:{
                from: "societies",
                localField: "societyCode",
                foreignField: "societyCode",
                as: "society",
            },
        },
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.items).aggregate(aggregation).toArray();
    }).then(formatResult);
}
