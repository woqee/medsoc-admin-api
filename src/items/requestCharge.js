/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import calculatePaymentTotal from './calculatePaymentTotal';
import httpAgent from '../httpAgent/httpAgent'
import {
    PAYMENT,
    createLog
} from '../activityLogs/activityLogs';
import R from 'ramda'
import bugsnag from '../bugsnag'

const env = process.env.NODE_ENV;

const PUBLIC_KEY = env === 'production' ? '40114e9f79a2a8b0c17cfc63264cae3d': 't_c6d07c14b18634cb0d58631631e15b';
const PRIVATE_KEY = env === 'production' ? 'fc28a2f94e807f432e4a3cc009baa747' : 't_2e27220aad1665111d1a9c878203c3';

type ChargeData = {
    amount: number,
    currency: string,
    browser_ip: string,
    browser_domain: string,
    description: string,
    email: string,
    token: string,
    customer: Array<*>,
}

export const requestCharge = (data: ChargeData) => {
    return httpAgent
    .post('https://api.paymentwall.com/api/brick/charge')
    .send(data)
    .set('X-ApiKey', PRIVATE_KEY)
    .set('Content-Type', 'application/x-www-form-urlencoded')
};

export default async ({
    items,
    token,
    user,
    ip,
    ipDomain = 'http://med-societies.com',
    bulkPaymentId,
}) => {
    let amount = 0;
    let currency = []

    items.map((item, index) => {
        const totalCost = calculatePaymentTotal(item).totalCost;

        currency.push(item.currency)
        amount += totalCost;
    });

    const chargeData = {
        amount,
        currency: R.uniq(currency)[0],
        browser_ip: ip,
        browser_domain: ipDomain,
        description: 'Med Societies Payments',
        plan: bulkPaymentId,
        email: user.email,
        token,
        customer: {
            firstname: user.firstName,
            lastname: user.lastName,
        },
    };

    // try {
    //     const result = await requestCharge(chargeData)
    //
    //     return result
    // } catch(error) {
    //
    //     bugsnag.notify(error)
    //
    //     return Promise.reject(error);
    // }

    return requestCharge(chargeData)
};
