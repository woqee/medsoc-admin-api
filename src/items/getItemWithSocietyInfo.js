import {aggregate} from '../mongodb'
import {COLLECTION} from '../mongodb/mongoConstant'
import {ObjectID} from 'mongodb'
import R from 'ramda'

const aggregateItems = aggregate(COLLECTION.items)

type Result = {
    _id: Object,
    itemName: string,
    price: number,
    createdAt: Object,
    isAbsorbBankingFees: boolean,
    paidAt: Object,
    societyCode: string,
}


export default (itemId: string): Promise<[Result]> => {
    const aggregation = [
        {
            $match: {
                _id: ObjectID(itemId)
            },
        },
        {
             $lookup:{
                 from: "societies",
                 localField: "societyCode",
                 foreignField: "societyCode",
                 as: "society",
             },
        },
    ]

    return aggregateItems(aggregation)
    .then((item) => R.head(item))
    .then((item) => ({
        ...item,
        society: {
            ...item.society[0]
        }
    }))
}
