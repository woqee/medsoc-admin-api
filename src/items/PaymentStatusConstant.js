/* @flow */



export const MEMBERSHIP_STATUSES = {
    nonMember: 'Non Member',
    associate: 'Associate',
    regular: 'Regular',
    fellow: 'Fellow',
    permanent: 'Permanent',
    'senior-fellow': 'Senior Fellow',
    'foreign-nonMember': 'Non Member - Foreigner Doctor',
};

export const PENDING = 'pending'
export const FELLOW = 'fellow'
export const NON_MEMBER = 'nonMember'
export const FOREIGN_NONMEMBER = 'foreign-nonMember'
