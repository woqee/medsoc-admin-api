/* @flow */

import mongoConnect from '../../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../../mongodb/mongoConstant';
import createLog from '../../activityLogs/insertUserLogs';
import logType from '../../activityLogs/LogTypes';

export default (query) => {
    const {itemId, item, userId, operatorId} = query

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.payments)
        .deleteOne({
            paymentType: logType.MANUAL_PAYMENT,
            userId: new ObjectID(userId),
            itemId: new ObjectID(itemId),
        });
    }).then((result) => {
        createLog({
            type: logType.REVERT_MANUAL_PAYMENT,
            triggeredBy: operatorId,
            logDate: new Date(),
            information: {
                userId,
                item: item
            },
        });
    });
};
