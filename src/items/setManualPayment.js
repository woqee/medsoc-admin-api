/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import createLog from '../activityLogs/insertUserLogs';
import logType from '../activityLogs/LogTypes';

export default ({itemId, item, userId, operatorId}) => {
    const payment = {
        paymentType: logType.MANUAL_PAYMENT,
        paidDate: new Date(),
        userId: new ObjectID(userId),
        itemId: new ObjectID(itemId),
        item: item,
        operatorId,
    }

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.payments)
        .insert(payment);
    }).then((result) => {
        createLog({
            type: logType.MANUAL_PAYMENT,
            triggeredBy: operatorId,
            logDate: new Date(),
            information: {
                userId,
                item: item
            },
        });
    });
};
