/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import {ObjectID} from 'mongodb';

export default (itemId: string) => {
    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.items).findOne({
            _id: ObjectID(itemId)
        });
    });
}
