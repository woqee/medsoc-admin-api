/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import {ObjectID} from 'mongodb';

export default (itemId: string, item: Object) => {
    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.payments).update({
            _id: ObjectID(itemId)
        }, {
            $set: {
                ...item,
            }
        }, {
            upsert: true,
        });
    });
}
