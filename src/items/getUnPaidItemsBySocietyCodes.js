/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import {ObjectID} from 'mongodb';
import {PENDING, NON_MEMBER} from './PaymentStatusConstant'
import Ramda from 'ramda';

const formatResult = Ramda.map((result) => ({
    ...result,
    logoURl: result.society[0].logoURL,
    currency: result.currency ? result.currency : 'PHP',
}))

export default (userId, societies: Array<Object>) => {
    const status = societies.reduce((acc, {active, isForeign, status, societyCode}) => {
        if(status === PENDING || !status) {
            return active ? acc : acc.concat({
                accessibleBy: NON_MEMBER,
                societyCode,
            });
        }

        return acc.concat({
            accessibleBy: active ? status : NON_MEMBER,
            societyCode,
        })
    }, [])

    const nonMembers = [{accessibleBy: 'nonMember'}]

    const aggregation = [
        {$match: {
            $or: [
                ...(Ramda.isEmpty(status) ? nonMembers : status),
                {societyCode: 'PMA'},
                {userId: ObjectID(userId)}
            ],
            archive: {
                $exists: false,
            },
        }},
         {
             $lookup:{
                 from: "payments",
                 localField: "_id",
                 foreignField: "itemId",
                 as: "paidItems",
             },
         },
         {
             $lookup:{
                 from: "societies",
                 localField: "societyCode",
                 foreignField: "societyCode",
                 as: "society",
             },
         },
         {$match: {
               $nor: [{'paidItems.userId': new ObjectID(userId) }],
         }},
         {$project: {
             itemName: 1,
             name: 1,
             societyCode: 1,
             isAbsorbBankingFees: 1,
             currency: 1,
             price: '$price',
             society: 1
         }}
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.items).aggregate(aggregation).toArray();
    }).then(formatResult);
}
