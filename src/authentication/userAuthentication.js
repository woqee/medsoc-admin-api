/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import passwordValidate from './passwordValidate';

export default ({name, pass}): Promise => mongoConnect.then((db) => {
    return db.collection('users').findOne({
        email: name,
    })
}).then((result) => {
    const {password} = result;

    return passwordValidate(password, pass)
    .then((validation) => {
        if(!validation) {
            return false;
        }

        return result;
    })
});
