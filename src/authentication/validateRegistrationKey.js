/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import passwordGenerate from './passwordGenerate';

export default (registrationKey: string): Promise => {
    return mongoConnect.then((db) => {
        return db.collection('users').findOne({registrationKey});
    });
}
