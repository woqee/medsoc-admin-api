/* @flow */

import bcrypt from 'bcryptjs';

export default (hash: string, password: string): Promise<*> => new Promise((resolve, reject) => {
    bcrypt.compare(password, hash, (error, success) => {
        if (error) {
            return reject(error);
        }

        return resolve(success);
    });
});
