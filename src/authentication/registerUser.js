/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import passwordGenerate from './passwordGenerate';

const createIndexForUsers = (db) => db.collection('users').createIndex({email: "text", firstName: "text" , lastName: "text", membershipStatus: "text"})

export default (user: BasicAuthCredentials): Promise => {
    return passwordGenerate(user.password).then((password) => (
        mongoConnect.then((db) => (
            db.collection('users').findAndModify(
                {
                    registrationKey: user.key,
                },
                [['_id','asc']],
                {
                    $set: {
                        firstName: user.firstName,
                        lastName: user.lastName,
                        specialization: user.specialization,
                        password: password,
                        active: true,
                    }
                },
                {
                    $new: true,
                }
            )
        ))
    ))
    .then((result) => {
        mongoConnect
        .then((db) => createIndexForUsers(db))

        return result
    })
}
