/* @flow */

import bcrypt from 'bcryptjs';

export default (password: string): Promise => new Promise((resolve, reject) => {
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(password, salt, function(error, hash) {
            if(error) {
                return reject(error);
            }

            return resolve(hash);
        });
    });
});
