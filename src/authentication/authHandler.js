import getUserFromRequest from 'basic-auth';
import userAuthentication from './userAuthentication';

type authCredential = {
    name: string,
    pass: string,
}

export default (req, scopes, definition, done) => {
    const notAuthorised = () => done({
        status: 401,
        challenge: 'Basic realm=medsoc',
        message: 'You must authenticate to access medsoc api',
    });

    const user: ?authCredential = getUserFromRequest(req);

    if (!user) {
        notAuthorised();
        return;
    }

    userAuthentication(user).then((isAuthenticated) => {
        if (!isAuthenticated) {
            notAuthorised();
            return;
        }

        req.userId = isAuthenticated._id;
        req.user = isAuthenticated;
        done(null, true);
    }).catch(notAuthorised);
};
