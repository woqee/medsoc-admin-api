/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID, Binary} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import fs from 'fs'


export default (fileId: File): Promise => {
    return mongoConnect
    .then((db) => {
        return db.collection(COLLECTION.files).findOne({
            _id: ObjectID(fileId),
        })
    })
}
