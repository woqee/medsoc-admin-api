/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID, Binary} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import fs from 'fs'


const fileToMongo = (file) => {
    const fileBinary = fs.readFileSync(file.path);
    const mongoBinary = new Binary(fileBinary)
    const fileData = {
        data: mongoBinary,
        type: file.type,
        size: file.size,
        contentType: file.headers['content-type'],
    }

    return {
        fileName: file.originalFilename,
        data: fileData,
    }
}

type File = {
    filePurpose: string,
    data: object,
}

export default (file: File): Promise => {

    const data = fileToMongo(file)

    return mongoConnect
    .then((db) => {
        return db.collection(COLLECTION.files).insert({
            fileName: data.fileName,
            data,
        })
    })
    .then((result) => result.ops[0])
}
