/* @flow */
import express from 'express';
import mongoConnect from './mongodb/mongoConnect';
import {COLLECTION} from './mongodb/mongoConstant';
import path from 'path';
import openapi from 'express-openapi';
import registerUser from './authentication/registerUser';
import userAuthentication from './authentication/userAuthentication';
import authHandler from './authentication/authHandler';
import createItem from './items/create/createItem';
import getItems from './items/getItems';
import getUnPaidItemsBySocietyCodes from './items/getUnPaidItemsBySocietyCodes';
import bodyParser from 'body-parser';
import cors from 'cors';
import {ObjectID, Binary} from 'mongodb';
import activityLogs from './activityLogs/activityLogs';
import bcrypt from 'bcryptjs';
import sendPaymentEmail from './items/sendPaymentEmail';
import R from 'ramda';
import compress from 'compression';
import mailgun from './mailgun';
import getUserFromRequest from 'basic-auth';
import multipart from 'connect-multiparty';
import fs from 'fs';
import createDoctorSociety from './society/createDoctorSociety'
import updatePayments from './payments/updatePayments'

const app = express();

const multipartmiddleware = multipart();
app.use(compress()); // This should come first
app.set('etag', 'strong');
app.set('json spaces', 4);

app.use(cors());
app.options('*', cors()); // Pre-flight CORS
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(multipartmiddleware);
openapi.initialize({
    apiDoc: require('./apiDoc').apiDoc,
    app,
    paths: path.join(__dirname, '/routes'),
    securityHandlers: {
        basicAuth: authHandler,
    },
});

openapi.initialize({
    apiDoc: require('./publicApiDoc').apiDoc,
    app,
    paths: path.join(__dirname, '/publicRoutes'),
    securityHandlers: {},
});

import getItemWithSocietyInfo from './items/getItemWithSocietyInfo'
import societyEmailReport from './payments/emailTemplate/societyEmailReport'
import getUserById from './user/getUserById'
import customerEmailConfirmation from './payments/emailTemplate/customerEmailConfirmation'


app.get('/items', async (req, res) => {
    // societyEmailReport({itemName: '13th Asia Pacific Congress of ELSA (Early Bird Rate)', email: 'pales_06@yahoo.com', societyName: 'Philippine Association of Laparoscopic and Endoscopic Surgeons', firstName: 'Benjamin', lastName: 'Souribio' })


    res.json('result')
})

//pay maya webhooks
app.get('/v1/pay/done', (req, res) => {
    res.send(req.query.status)
})

import {insert} from './mongodb'

const insertHook = insert(COLLECTION.hooks)

app.post('/pay/hooks/success', async (req, res) => {
    try {
        await updatePayments(req.body)

    } catch (e) {
        res.status(201).send('OK')
    }

    res.status(201).send('OK')
})

app.post('/pay/hooks/failure', (req, res) => {
    mongoConnect.then((db) => db.collection(COLLECTION.hooks)
    .insert({
        type: 'failure',
        data: req.body
    }))


    res.status(201).send('OK')
})

app.post('/pay/hooks/dropped', (req, res) => {
    mongoConnect.then((db) => db.collection(COLLECTION.hooks)
    .insert({
        type: 'dropped',
        data: req.body
    }))

    res.status(201).send('OK')
})


// end of paymaya hooks


import updateUserInfo from './user/updateUserInfo'
app.post('/v1/save/profile', (req, res) => {
    const {profileImg} = req.files
    const {id} = req.body

    const properties = {
        profileImg: profileImg ? profileImg : null,
    }

    updateUserInfo(id, properties).then(() => {
        res.send('updated')
    }).catch(() => res.status(500).send('error uploading'))
})
import serveFile from './files/serveFile'
app.get('/files/:fileId', (req, res) => {
    const {fileId} = req.params

    serveFile(fileId).then((result) => {
          res.writeHead(200, {
              'Content-Type': result.data.data.type,
              'Content-Length': result.data.data.size,
          });
          res.write(result.data.data.data.buffer);
          res.end()
    }).catch(() => res.status(404).end())
})

app.get('/headers', (req, res) => {
    res.json(req.headers)
})

import csv from 'fast-csv'
app.post('/batch/users/', (req, res) => {
    const {users} = req.files
    fs.createReadStream(users.path)
    .pipe(csv())
    .on("data", function(data){
        if(data[0] === '') {
            return;
        }

        const salt = bcrypt.genSaltSync(10);
        const name = data[0].split(',')
        console.log(data);
        const firstName = name[1] ? `${name[1]}` : ''
        const lastName = name[0] ? `${name[0]}` : ''
        const status = data[2].toLowerCase()
        const inviteFormat = {
            email: data[1],
            type: 'doctor',
            membershipStatus: 'associate' ,
            active: false,
            firstName: firstName,
            lastName: lastName,
            dateCreated: new Date(),
            registrationKey: salt,
        };

        mongoConnect.then((db) => db.collection(COLLECTION.users)
        .insert(inviteFormat))
        .then((result) => {
            const society = {
                _id: ObjectID("574c43af012f4575f1facb25"),
                status: 'associate',
                societyCode: 'PALES',
            }
            return createDoctorSociety(result.ops[0]._id, society);
        })
    })
    .on("end", function(){
        res.send('ok')
    });

})

import getWebUrlByEnvironment from './getWebUrlByEnvironment'

import puaTemplate from './user/societyTemplateForUserInvitation/pua'
import palesTemplate from './user/societyTemplateForUserInvitation/pales'

const PALES = 'PALES'
const PUA = 'PUA'

const mailTemplate = {
    [PALES]: palesTemplate,
    [PUA]: puaTemplate,
}

import getDoctorsBySocietyCode from './user/getDoctorsBySocietyCode'

import itemEmail from './items/create/emailTemplate'

app.get('/mock', async (req, res) => {
    const doctors = await getDoctorsBySocietyCode('PUA', {
        status: 'fellow',
    })

    doctors.forEach(({firstName, lastName, email}) => {
        itemEmail({
            firstName,
            lastName,
            email,
            itemName: 'PUA Annual Convention 2017',
            societyName: 'Philippine Urological Association',
        })
    })

    res.json(doctors)

})

app.get('/batch/invite', (req, res) => {
    getDoctorsBySocietyCode('PALES', {}, false)
    .then((users) => {
        users.map((result) => {

            const societyTemplate = mailTemplate[PALES]

            const emailHtml = societyTemplate({
                email: result.email,
                name: result.firstName,
                registrationKey: result.registrationKey,
            })

            mailgun(emailHtml, result.email);
        })

        res.status(201).send('invite successful for pales')
    })
})
app.get('/v1/society/image/', (req, res) => {
    const {societyCode} = req.query
})

import passwordRecovery from './publicRoutes/passwordRecovery'
import resetpassword from './publicRoutes/resetpassword'
import generatePAssword from './authentication/passwordGenerate'
app.get('/v1/user/password-recovery', passwordRecovery)
app.post('/v1/user/reset-password', resetpassword)
app.get('/pd', async (req, res) => {
    const {
        password
    } = req.query

    const pw = await generatePAssword(password)
    res.send(pw)
})
app.post('/v1/society/add', multipartmiddleware, (req, res) => {
    const {
        societyName,
        societyCode,
        type,
    } = req.body
    const {logoURL} = req.files

    const binaryData = fs.readFile(logoURL.path, (err, imageData) => {
        mongoConnect.then((db) => {
            return db.collection(COLLECTION.societies).insert({
                societyName,
                societyCode,
                type,
                logoURL: {
                    fileName: logoURL.originalFilename,
                    type: logoURL.type,
                    size: logoURL.size,
                    binary: Binary(imageData),
                }
            })
        })
        .catch((err) => {
            console.log(err)
        })
    })



    res.status(200).send({
        societyName,
    })
})

import validateRegistrationKey from './authentication/validateRegistrationKey';
app.get('/v1/register/validate', (req, res) => {
    const {key} = req.query;

    validateRegistrationKey(key)
    .then((result) => {
        if(!result.active) {
            res.status(200).json({
                ...result,
            })

            return;
        }

        res.status(401).json({
            error: 'User is already Registered',
        })

        return;
    })
    .catch((error) => {
        res.status(400).json({
            error: 'Invalid key',
        })
    })
});

app.get('/v1/logs', (req, res) => {
    if(req.query) {
        const operatorId = req.query.operatorId;

        mongoConnect.then((db) => {
            return db.collection(COLLECTION.logs).find({
                operator: operatorId,
            }).toArray();
        }).then((result) => {
            res.status(200).json(result);
        });
    }
});

app.post('/v1/item/disabled/:itemId', (req, res) => {
    const {itemId} = req.params;

    setItemById(itemId, {
        archive: true,
    }).then(() => {
        res.status(201).json({})
    })
})

import getUserByEmail from './user/getUserByEmail'
app.get('/earlybird', async (req, res) => {
    // return registerUser({
    //     ...req.body,
    // }).then((result) => {
    //     return result.value
    // })
    // .then((result) => {
    const results = await getDoctorsBySocietyCode('PALES', {}, false)

    // res.json(results)
    // return
    results.map((result) => {
        const emailTemplate = {
            from: 'Med-Societies Support <support@med-societies.com',
            to: result.email,
            subject: 'Med Societies Reminder',
            html: `
            <div style="color: gray; font-weight: 200;font-family:arial;font-size: 13px;margin: 60px">
             <div style="margin-bottom: 20px;padding-bottom: 10px;border-bottom: 1px solid gray">
              <img
                   style="height: 40px"
                   src='https://s3-ap-southeast-1.amazonaws.com/societies/logos/Med_Soc_Logo.png'/>
             </div>
           Dear Dr. ${result.firstName}
             <br/>
             <br/>
             Last day for the Early Bird Rates of the 13th Asia Pacific Congress of ELSA. You can buy your ticket now on the Med Societies mobile app. This event will be hosted by the Philippine Association of Laparoscopic and Endoscopic Surgeons (PALES) on November 16-18, 2017 at Shangri-La Mactan.
 <br/> <br/>
             The app is available for download from the <a href="https://itunes.apple.com/ph/app/med-societies/id1122588574?mt=8"> App Store</a> or <a href="https://play.google.com/store/apps/details?id=com.medsociety" >Play Store</a>.
             <br/>
             <br/>
             Thanks and see you
             <br/>
             Med-Societies Team
             <br/>
             <br/>
             <br/>
             <span style='font-family:"open sans","source sans pro",verdana,tahoma,arial;font-size:10px;color:rgb(67,67,67);background-color:rgb(251,251,251);text-align:center;display:block'>
               <strong style='text-align:center;'>ABOUT THIS EMAIL: </strong><br/>
               You are receiving this message under the email address ${result.email} as a service from Med-Societies.
       <br/>
       Copyright © 2018 Woqee.com, Inc. | 2/F Amina Cuilding, Tandang Sora Ave., 1116 Quezon City, Philippines.

             </span>
         </div>
            `
        }

        mailgun(emailTemplate, result.email)
    })
    res.send('ok')

    return


        res.status(201).json(result);
    // })
    // .catch((error) => {
    //     console.log(error);
    // });
});

app.post('/v1/register', (req, res) => {
    return registerUser({
        ...req.body,
    }).then((result) => {
        return result.value
    })
    .then((result) => {
        const emailTemplate = {
            from: 'Med-Societies Support <support@med-societies.com',
            to: result.email,
            subject: 'Successfully Registered',
            html: `
            <div style="color: gray; font-weight: 200;font-family:arial;font-size: 13px;margin: 60px">
             <div style="margin-bottom: 20px;padding-bottom: 10px;border-bottom: 1px solid gray">
              <img
                   style="height: 40px"
                   src='https://s17.postimg.org/iqbgj1jr3/Med_Soc_Logo.png'/>
             </div>
           Dear Dr. ${result.firstName}
             <br/>
             <br/>
             Thank you for registering on Med Societies. To begin, please download the app on either the <a href="https://itunes.apple.com/ph/app/med-societies/id1122588574?mt=8"> App Store</a> or <a href="https://play.google.com/store/apps/details?id=com.medsociety" >Play Store</a> on your mobile device.
             <br/>
             <br/>
             After downloading, login using this email ${result.email} and your password from registration.
             <br/>
             <br/>
             Warmest Regards,
             <br/>
             Med-Societies Team
             <br/>
             <br/>
             <br/>
             <span style='font-family:"open sans","source sans pro",verdana,tahoma,arial;font-size:10px;color:rgb(67,67,67);background-color:rgb(251,251,251);text-align:center;display:block'>
               <strong style='text-align:center;'>ABOUT THIS EMAIL: </strong><br/>
               You are receiving this message under the email address ${result.email} as a service from Med-Societies.
       <br/>
       Copyright © 2018 Woqee.com, Inc. | 2/F Amina Cuilding, Tandang Sora Ave., 1116 Quezon City, Philippines.

             </span>
         </div>
            `
        }

        mailgun(emailTemplate, result.email)

        res.status(201).json(result);
    })
    .catch((error) => {
        console.log(error);
    });
});

app.get('/b/email', (req, res) => {
    mongoConnect.then((db) => db.collection('users')
    .find({active: true, type: 'doctor'})
    .toArray())
    .then((user) => {
        user.map((result) => {
             const emailTemplate = {
                 from: 'Med-Societies Support <support@med-societies.com',
                 to: result.email,
                 subject: 'Successfuly Registered',
                 html: `
                 <div style="color: gray; font-weight: 200;font-family:arial;font-size: 13px;margin: 60px">
                  <div style="margin-bottom: 20px;padding-bottom: 10px;border-bottom: 1px solid gray">
                   <img style="height: 40px"
                        src='https://s17.postimg.org/iqbgj1jr3/Med_Soc_Logo.png'/>
                  </div>
                Dear Dr. ${result.firstName}
                  <br/>
                  <br/>
                  Thank you for registering on Med Societies. To begin, please download the app on either the <a href="https://itunes.apple.com/ph/app/med-societies/id1122588574?mt=8"> App Store</a> or <a href="https://play.google.com/store/apps/details?id=com.medsociety" >Play Store</a> on your mobile device.
                  <br/>
                  <br/>
                  After downloading, login using this email ${result.email} and your password from registration.
                  <br/>
                  <br/>
                  Warmest Regards,
                  <br/>
                  Med-Societies Team
                  <br/>
                  <br/>
                  <br/>
                  <span style='font-family:"open sans","source sans pro",verdana,tahoma,arial;font-size:10px;color:rgb(67,67,67);background-color:rgb(251,251,251);text-align:center;display:block'>
                    <strong style='text-align:center;'>ABOUT THIS EMAIL: </strong><br/>
                    You are receiving this message under the email address ${result.email} as a service from Med-Societies.
            <br/>
            Copyright © 2016 Woqee.com, Inc. | 2/F Amina Cuilding, Tandang Sora Ave., 1116 Quezon City, Philippines.

                  </span>
              </div>
                 `
             }
             //
             mailgun(emailTemplate, result.email)
        })
        res.status(201).json({});
    })
})

import getRequiredItems from './items/getRequiredItem';
app.get('/v1/items/required', (req, res) => {
    const {societyCode} = req.query;

    getRequiredItems(societyCode)
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            consoe.log('/v1/items/required' , err);
            res.status(500).json({message: 'unable to process request'});
        });
});


import getItemById from './items/getItemById';
import setItemById from './items/setItemById';
app.get('/v1/items/:itemId', (req, res) => {
    const {itemId} = req.params;

    getItemById(itemId)
    .then((result) => {
        res.status(200).json(result);
    })
    .catch((err) => {
        res.status(301).json({message: 'unable to process request'});
    });
});

app.post('/v1/items/:itemId', (req, res) => {
    const {itemId} = req.params;
    const item = req.body;

    setItemById(itemId, item)
    .then((result) => {
        res.status(200).json({message: 'Success'});
    })
    .catch((error) => {
        console.log('post /v1/items/:itemId', error);
        res.status(301).json({message: 'unable to process request'});
    });
});

app.get('/v1/items', (req, res) => {
    const {societyCode} = req.query;

    getItems(societyCode)
        .then((result) => {
            res.status(200).json(result);
        })
        .catch((error) => {
            console.log('items', error);
            res.status(301).json({message: 'unable to process request'});
        });
});

import getDoctorSociety from './society/getDoctorSociety';

app.post('/v1/itemPayments', (req, res) => {
    const {_id} = req.body;

    getDoctorSociety(_id)
    .then((societyCodes) => getUnPaidItemsBySocietyCodes(_id, societyCodes))
    .then((result) => {
        res.json({ items: result});
    }).catch((e) => {
        res.status(400).json({e});
    });
});

import setDoctorSociety from './society/setDoctorSociety';
import getSocietiesByCode from './society/getSocietiesByCode';
import updateUserSocietyStatus from './society/updateUserSocietyStatus';
import getDoctorBySociety from './user/getDoctorBySociety';


app.get('/v1/user/:userId', (req, res) => {
    const {userId} = req.params;
    const {societyCode} = req.query;

    getDoctorBySociety(userId, societyCode)
    .then((result) => {
        res.status(200).json(result);
    })
    .catch((error) => {
        res.status(400).json(error);
    })
});

app.post('/v1/user/status', (req, res) => {
    const {_id, societyCode, status} = req.body;
    updateUserSocietyStatus(_id, societyCode, status)
    .then(() => {
        res.status(202).json('success');
    });
    res.status(202).json('success');
});


app.post('/v1/userSociety', (req, res) => {
    const {_id, societies} = req.body;
    setDoctorSociety(_id, societies)
        .then((result) => {
            res.status(201).json('Saved');
        });
});


/* PAYMENT WALL PINGBACK */
import Paymentwall from 'paymentwall';
import getIpString from './common/getIpString';
import validatePingback from './common/paymentwall/validatePingback';

const PUBLIC_KEY = 't_c6d07c14b18634cb0d58631631e15b';
const PRIVATE_KEY = 't_2e27220aad1665111d1a9c878203c3';
const MEDSOC_SECRET = 'fc28a2f94e807f432e4a3cc009baa747';

const getType = {
    [200]: 'pending',
    [201]: 'accepted',
    [202]: 'declined',
}

const getIp = (request) => {
    const ip = request.headers['x-forwarded-for'] ||
     request.connection.remoteAddress ||
     request.socket.remoteAddress ||
     request.connection.socket.remoteAddress;

    return getIpString(ip);
}

app.get('/v1/pingback', (req, res) => {

    const {
        type,
        goodsid,
        ref,
        sig,
        uid,
    } = req.query;

    const validated = validatePingback(req.query, getIp(req), MEDSOC_SECRET);

    mongoConnect.then((db) => db.collection(COLLECTION.logs).insert({
        type: 'pingback',
        bulkPaymentId: goodsid,
        pingbackDetail: {
            transaction: getType[type],
            type,
            ref,
            sig,
            ip: getIp(req),
            uid,
        },
    }))

    if (validated) {
        if(getType[type] === 'accepted') {
            mongoConnect.then((db) =>
                db.collection(COLLECTION.payments)
                .update(
                    {'bulkPaymentId': goodsid},
                    {
                        $set: {
                            'chargeInfo.risk': 'approved',
                            'chargeInfo.approvedDate': new Date(),
                        }
                    },
                    {multi: true}
                )
            )
            res.status(200).send('OK');
        } else if(getType[type] === 'declined') {
            mongoConnect.then((db) =>
                db.collection(COLLECTION.payments)
                .update(
                    {'bulkPaymentId': goodsid},
                    {
                        $set: {
                            'chargeInfo.risk': 'declined',
                        }
                    },
                    {multi: true}
                )
            )
            res.status(200).send('OK');
        }
        res.status(200).send('OK');
    } else {
        res.status(300).send('NOT OK');
    }
});

// import {insert, find} from '../src/mongodb'
//
// const insertUserSociety = insert(COLLECTION.userSociety)
// app.get('/pcssetup', async (req, res) => {
//     const users = await find(COLLECTION.users, {type: 'doctor'})
//
//     users.map((user) => {
//         insertUserSociety({
//             userId: user._id,
//             societyId : ObjectID('59393d70a1002be6a3e28565'),
//             societyCode : 'PCS',
//             status : 'pending',
//             active : true,
//             createdAt : new Date()
//         })
//     })
//
//     res.status(200).send('OK')
// })

app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
    console.log(err); // eslint-disable-line no-console
    res.status(err.status || 500).json(err.message || err);
});

Promise.all([
    mongoConnect,
]).then(async () => {
    const env = process.env.NODE_ENV;

    if(env === 'production') {
        console.log('Server started at port 8080 prod'); // eslint-disable-line no-console
        app.listen(8080);
    }

    if(env === 'development') {
        console.log('Server started at port 8080 dev'); // eslint-disable-line no-console
        app.listen(8080);
    }

    if(env === 'staging') {
        console.log('Server started at port 8080 staging'); // eslint-disable-line no-console
        app.listen(3000);
    }

    if(env === 'demo') {
        console.log('Server started at port 8081 staging'); // eslint-disable-line no-console
        app.listen(8081);
    }

}).catch((errors) => {
    console.log('Could not connect to database', errors); // eslint-disable-line no-console
});
