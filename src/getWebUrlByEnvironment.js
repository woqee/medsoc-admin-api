/* @flow */

import R from 'ramda'

const URL_PRODUCTION = 'https://admin.med-societies.com'
const URL_DEMO = 'https://demo-admin.med-societies.com'
const LOCAL = 'http://localhost:3000'

export default (parameter: string): string => {
    const env = process.env.NODE_ENV

    const webURL = R.cond([
        [R.equals('production'), R.always(URL_PRODUCTION)],
        [R.equals('demo'), R.always(URL_DEMO)],
        [R.T, R.always(URL_DEMO)],
    ])

    return `${webURL(env)}${parameter}`
}
