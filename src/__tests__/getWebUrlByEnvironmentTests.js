/* @flow */

import getWebUrlByEnvironment from '../getWebUrlByEnvironment'

describe('Web URL By Environment', () => {

    it('returns the url of the client/front end if production', () => {
        process.env.NODE_ENV = 'production'

        expect(getWebUrlByEnvironment('/register')).toEqual('https://admin.med-societies.com/register')
    })

    it('returns the url of the client/front end if local', () => {
        process.env.NODE_ENV = 'demo'

        expect(getWebUrlByEnvironment('/register')).toEqual('https://demo-admin.med-societies.com/register')
    })
})
