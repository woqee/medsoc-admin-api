/* @flow*/

export const apiDoc = {
    swagger: '2.0',
    basePath: '/v1',
    info: {
        title: 'Medsoc API',
        version: '1.0.0',
    },
    definitions: {
        Error: {
            additionalProperties: true,
        },
    },
    paths: {},
    securityDefinitions: {
        basicAuth: {
            type: 'basic',
        },
    },
    security: [
        {
            basicAuth: [],
        },
    ],
}
