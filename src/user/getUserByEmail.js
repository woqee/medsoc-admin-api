/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (email) => {
    return mongoConnect.then((db) =>
        db.collection(COLLECTION.users).findOne({
            email,
        })
    )
}
