/* @flow */

import {aggregate} from '../mongodb';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import R from 'ramda'

const aggregateUserSociety = aggregate(COLLECTION.userSociety)
const aggregateUsers = aggregate(COLLECTION.users)

const mapToObjectId = R.map(R.evolve({
    _id: ObjectID,
}))

const formatResult = R.map(R.pick(['email', 'firstName', 'lastName', 'registrationKey']))

export default async (societyCode: string, match?: Object, active?: boolean = true) => {
    const userIds = await aggregateUserSociety([
        {
            $match: {
                societyCode: societyCode,
                ...match,
                $or: [
                    {societyCode: 'PMA'}
                ],
            },
        },
        {
            $group: {
                _id: '$userId'
            },
        },
    ]);

    const userIDs = mapToObjectId(userIds)

    const users = await aggregateUsers([
        {
            $match: {
                active,
                $or: userIDs,
                active: true,
            }
        }
    ])

    return formatResult(users)
}
