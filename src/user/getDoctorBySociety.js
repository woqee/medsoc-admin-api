/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (userId: string, societyCode: string) => {
    const aggragation = [
        {$match: {
            userId: new ObjectID(userId),
            societyCode: societyCode,
        }},
        {$lookup:{
            from: "users",
            localField: "userId",
            foreignField: "_id",
            as: "user",
        }}
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.userSociety)
            .aggregate(aggragation).toArray();
    });
}
