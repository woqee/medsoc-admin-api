/* @flow */

import mailgun from '../mailgun';
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (passwordRecoveryToken): Promise =>
    mongoConnect
    .then((db) =>
        db.collection(COLLECTION.users).findOne({
            passwordRecoveryToken,
        })
    )
