/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (userId) => {
    const id = typeof userId === 'string' ? ObjectID(userId) : userId

    return mongoConnect.then((db) =>
        db.collection(COLLECTION.users).findOne({
            _id: ObjectID(id)
        })
    )
}
