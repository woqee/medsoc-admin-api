/* @flow */

import getWebUrlByEnvironment from '../../getWebUrlByEnvironment'

type EmailData = {
    email: string,
    name: string,
    registrationKey: string,
}

export default (emailData) => {
    const webUrl = getWebUrlByEnvironment('/register')
    const {
        email,
        name,
        registrationKey,
    } = emailData
    const withQuery = `${webUrl}?key=${registrationKey}`

    return {
      from: 'Philippine Urological Association <pua@med-societies.com>',
      to: email,
      subject: 'Med Society App Admin Invitation',
      html: `
      <br/>
      Dear Dr. ${name},
      <br/>
      <br/>
      It is with great pleasure that the Philippine Urological Association introduce the newly launched online payment system for all association-related dues: the Med Societies Mobile App.
      <br/>
      <br/>
      In order to use this new system, simply go to this unique <a href="${withQuery}">link</a>
    and complete your registration by filling out your profile information required, then download the app on your iPhone from the App Store or on your Android phone from the Play Store.
      <br/>
      <br/>
      If you can’t use the link above, please copy the url below and paste it on your browser.
      <br/>
      <a href=${withQuery}>${withQuery}</a>
      <br/>
      <br/>
      <br/>
      Constantino T. Castillo III
      <br/>
      Chairman of the Membership Committee
      <br/>
      Philippine Urological Association
      <br/>
      <br/>
      <br/>
      Gavino N. Mercado Jr.
      <br/>
      President
      <br/>
      Philippine Urological Association
      <br/>
      <br/>

      <div style='font-size:12.8px;'>-------------------------------------------------------------------------------</div>
      <br/>
       <div style='font-size:12.8px;'>This is a system-generated email. To reply to this or if you have questions, please email pua_org@yahoo.com</div>
      `,
    };
}
