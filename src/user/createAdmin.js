/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import passwordGenerate from '../authentication/passwordGenerate';
import getSocietyByCode from '../society/getSocietyByCode';
import shorid from 'shortid';

type Query = {
    username: string,
    firstName: string,
    lastName: string,
    societyCode: string,
    type: string,
    password: string,
}

export default async (query: Query): Promise<*> => {
    const {
        firstName = '',
        lastName = '',
        societyCode,
        password,
        email,
        username,
    } = query

    const {_id} = await getSocietyByCode(societyCode)
    const hashPassword = await passwordGenerate(password)

    const db = await mongoConnect

    return db.collection(COLLECTION.users).insert({
        email,
        firstName,
        lastName,
        username,
        society: _id,
        password: hashPassword,
        type: 'admin',
        active: true,
    });
}
