/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import getDoctorsPaidItem from '../items/getDoctorsPaidItem';
import R from 'ramda'

const addSearch = (search) => {
    if(R.isEmpty(search) || R.isNil(search)) {
        return {
            $match: {
                type: 'doctor',
            },
        }
    }

    return {
        $match: {
            $text: {
                $search: search,
            },
            type: 'doctor',
        },
    }
}

export default (societyCode: string, search: string) => {
    const aggregation = [
        addSearch(search),
        {
             $lookup:{
                 from: "payments",
                 localField: "_id",
                 foreignField: "userId",
                 as: "payments",
             },
         },
         {
              $lookup:{
                  from: "userSociety",
                  localField: "_id",
                  foreignField: "userId",
                  as: "society",
              },
         },
        {$unwind: '$society'},
        {$match: {
            'society.societyCode': societyCode,
            'society.active': true,
        }},
        {$project: {
                active: 1,
                dateCreated : 1,
                email : 1,
                firstName : 1,
                lastName : 1,
                membershipStatus : 1,
                specialization : 1,
                type : 1,
                payments: 1,
                society: 1,
        }},
    ];

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.users).aggregate(aggregation).toArray();
    });
}
