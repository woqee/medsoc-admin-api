/* @flow */

import mailgun from '../mailgun';
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import passwordGenerate from '../authentication/passwordGenerate'
import bcrypt from 'bcryptjs';
import findUserByResetPasswordToken from './findUserByResetPasswordToken'

export default (body): Promise => {
    const {
        password,
        passwordRecoveryToken,
    } = body


    return findUserByResetPasswordToken(passwordRecoveryToken)
    .then((user) => {
        if(user) {
            return passwordGenerate(password)
        }

        return Promise.reject({message: 'reset link is not valid'})
    })
    .then((passwordGenerated) => {
        return mongoConnect
        .then((db) =>
            db.collection(COLLECTION.users).update({
                passwordRecoveryToken,
            }, {
                $set: {
                    passwordRecoveryToken: null,
                    password: passwordGenerated,
                },
            })
        )
    })
}
