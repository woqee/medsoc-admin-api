/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import bcrypt from 'bcryptjs';

type User = {
    email: string,
    type: string,
    membershipStatus: string,
    active?: boolean,
    firstName: string,
    registrationKey: string,
    countryCode?: string,
}

export default (user: User) => {
    const salt = bcrypt.genSaltSync(10);

    const inviteFormat = {
        email: user.email,
        type: 'doctor',
        membershipStatus: user.countryCode === 'PH' ? user.membershipStatus : 'nonMember',
        active: false,
        firstName: user.firstName,
        dateCreated: new Date(),
        registrationKey: salt,
        countryCode: user.countryCode,
    }

    return mongoConnect.then((db) => {
        return db.collection(COLLECTION.users).insert(inviteFormat)
    })
    .then((result) => result.ops[0])
}
