/* @flow */

import mailgun from '../mailgun';
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID, Binary} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';
import fs from 'fs'
import saveFile from '../files/saveFile'

export default (_id: string, properties: object): Promise => {
    return mongoConnect
    .then((db) =>
        db.collection(COLLECTION.users).update({
            _id: ObjectID(_id),
        }, {
            $set: {
                ...properties,
            },
        })
    )

}
