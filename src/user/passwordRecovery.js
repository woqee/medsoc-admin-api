/* @flow */

import mailgun from '../mailgun';
import mongoConnect from '../mongodb/mongoConnect';
import {ObjectID} from 'mongodb';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (email: string, passwordRecoveryToken: string): Promise => {
    return mongoConnect
    .then((db) =>
        db.collection(COLLECTION.users).update({
            email,
        }, {
            $set: {
                passwordRecoveryToken,
            },
        })
    )
}
