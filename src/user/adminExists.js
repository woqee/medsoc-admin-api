/* @flow */
import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import passwordGenerate from '../authentication/passwordGenerate';
import getSocietyByCode from '../society/getSocietyByCode';
import {ObjectID} from 'mongodb';

type User = {
    username: string,
    email: string,
    societyCode: string,
}

export default async (user: User): Promise => {
    const {
        username,
        email,
        societyCode,
    } = user;
    const {_id} = await getSocietyByCode(societyCode)
    console.log(user)
    const db = await mongoConnect
    console.log(_id)
    return db.collection(COLLECTION.users).findOne({
        username,
        email,
        society: ObjectID(_id),
    });
}
