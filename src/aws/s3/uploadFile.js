import configuredS3 from './configuredS3'

type File = {
    body: any,
    name: string,
}

export default (bucket: string, file: File, acl: string = 'public-read'): Promise<*> =>
    new Promise((resolve, reject) => {
        configuredS3.putObject(
            {
                ACL: acl,
                Body: file.body,
                Bucket: bucket,
                Key: file.name,
            },
            (error: Error, result: any) => {
                if (error) {
                    reject(error)

                    return
                }

                resolve(result)
            }
        )
    })
