import AWS from 'aws-sdk'

export default new AWS.S3({
    apiVersion: '2006-03-01',
    accessKeyId: process.env.AWS_ACCESS_KEY, // eslint-disable-line no-process-env
    secretAccessKey: process.env.AWS_SECRET_KEY, // eslint-disable-line no-process-env
    region: 'ap-southeast-1',
})
