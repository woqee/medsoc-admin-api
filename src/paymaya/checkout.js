import httpAgent from '../httpAgent/httpAgent'
import base64 from 'base-64'

const env = process.env.NODE_ENV;

const PUBLIC_KEY = env === 'production' ? 'pk-DEFLPpr2I2o3pM4qTagrI760zNDjJox0ibnvtAJ0j8z': 'pk-nRO7clSfJrojuRmShqRbihKPLdGeCnb9wiIWF8meJE9';
const PRIVATE_KEY = env === 'production' ? 'sk-ybweLB4hxgSj4m4044w7zobYjBVRie2WyNtkY31m7nU' : 'sk-jZK0i8yZ30ph8xQSWlNsF9AMWfGOd3BaxJjQ2CDCCZb';

const checkoutURL = env === 'production' ? 'https://pg.paymaya.com/checkout/v1/checkouts' : 'https://pg-sandbox.paymaya.com/checkout/v1/checkouts'

export const requestCheckout = (data, publicKey) => {
    return httpAgent
    .post(checkoutURL)
    .send(data)
    .set('Authorization', `Basic ${publicKey}`)
    .set('Content-Type', 'application/json')
};

export default async (data) => {
    const publicKey = base64.encode(`${PUBLIC_KEY}:`)

    const result = await requestCheckout(data, publicKey)

    return JSON.parse(result.text)
}
