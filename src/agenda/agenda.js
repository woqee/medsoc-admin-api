/* @flow */

import Agenda from 'agenda'

export default () => {
    const MONGO_URL_LOCAL = 'mongodb://localhost:27017/medsoc_staging';
    const MONGO_URL_PROD = 'mongodb://medsocuser:woq239eee2@188.166.212.59:27017/medsoc';
    const MONGO_URL_STAGING = 'mongodb://188.166.212.59:27017/medsoc_dev';
    const MONGO_URL_DEMO = 'mongodb://medsocuser:woq239eee2@188.166.212.59:27017/medsoc_demo';

    const env = process.env.NODE_ENV;

    let MONGO_URL = '';
    if(env == 'production') {
        MONGO_URL = MONGO_URL_PROD
    }

    if(env === 'staging') {
        MONGO_URL = MONGO_URL_STAGING
    }

    if(env === 'development') {
        MONGO_URL = MONGO_URL_LOCAL
    }

    if(env === 'demo') {
        MONGO_URL = MONGO_URL_DEMO
    }

    const agenda = new Agenda({db: {address: MONGO_URL}});

    return agenda
}
