/* @flow */

import agenda from './agenda'
import registrationInvite from '../batch/registrationInvite'

export default () => {
    agenda.define('sendRegistrationInvite', (job, done) => {
        registrationInvite.then(() => done())
    })

    return agenda
}
