/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';
import setPayments from '../../items/setPayments';
import getIpString from '../../common/getIpString';
import getItemByIds from '../../items/getItemByIds';
import requestCharge from '../../items/requestCharge';
import shortid from 'shortid';
import getArrearsByIds from '../../arrears/getArrearsByIds'
import bugsnag from '../../bugsnag'
import paymayaCheckout from '../../paymaya/checkout'
import getCartPayment from '../../cart/getPayment'
import getUserById from '../../user/getUserById'
import getPaymentItemByCartId from '../../cart/getPaymentItemByCartId'
import R from 'ramda'

const requestParameters = [
    {
        name: 'body',
        required: false,
        in: 'body',
        schema: {
            type: 'object',
            properties: {
                token: {
                    type: 'string',
                },
                items: {
                    type: 'array',
                },
            },
        },
    },
];

const responseSchema = {};
const spec = {
    parameters: requestParameters,
    responses: {
        '201': {
            description: 'Payment completed',
            schema: responseSchema,
        },
        '406': {
            description: 'Payment cannot be completed',
            schema: responseSchema,
        },
    },
};

const getIp = (request) => {
    const ip = request.headers['x-forwarded-for'] ||
     request.connection.remoteAddress ||
     request.socket.remoteAddress ||
     request.connection.socket.remoteAddress;

    return getIpString(ip);
}
// paymaya checkout

const getSpec = {
    parameters: [],
    responses: {
        '201': {
            description: 'Payment completed',
            schema: {},
        },
        '406': {
            description: 'Payment cannot be completed',
            schema: {},
        },
    },
};

const env = process.env.NODE_ENV;

export const get = createEndPoint(getSpec, async ({request, response}) => {
    const {id} = request.query;
    const cartItems = await getPaymentItemByCartId(id)
    const user = await getUserById(cartItems.userId)

    const ip = getIp(request);
    const baseURL = 'https://staging.med-societies.com'

    const billingAddress = user.address ? {
        'line1': user.address.line1,
        'city': user.address.city,
        'state': user.address.state,
        'countryCode':  user.address.country || 'PH'
    } : {
        'line1': 'n/a',
        'city': 'n/a',
        'state': 'Metro Manila',
        'countryCode': 'PH'
    }

    if(R.isEmpty(cartItems.totalItemCount)) {
        return response.redirect('/')
    }

    const result = await paymayaCheckout({
        'totalAmount': {
            'currency': 'PHP',
            'value': cartItems.totalPrice.toFixed(2),
            'details': {
                'subtotal': cartItems.totalPriceWithoutFees.toFixed(2),
              'serviceCharge': cartItems.totalTransactionFees.toFixed(2),
            }
        },
        'buyer': {
            'firstName': user.firstName,
            'lastName': user.lastName,
            'contact': {
              'email': user.email
            },
            billingAddress,
            'ipAddress': ip
        },
        'items': cartItems.items.map((item) => ({
            'name': item.itemName,
            'description': `${item.itemName} ${item.societyName}`,
            'quantity': '1',
            'totalAmount': {
                'value': item.actualPrice.toFixed(2),
            }
        })),
        'redirectUrl': {
            'success': `${baseURL}/shop?status=success&complete=ok&id=${cartItems._id}`,
            'failure': `${baseURL}/shop?status=failure&id=${cartItems._id}`,
            'cancel': `${baseURL}/shop?status=cancel&id=${cartItems._id}`
        },
        'requestReferenceNumber': cartItems._id,
        'metadata': {}
    })

    return response.redirect(result.redirectUrl)
})
