/* @flow */
import createEndPoint, {formatResult} from '../createEndpoint';
import getUserByEmail from '../user/getUserByEmail';
import mailgun from '../mailgun';
import createDoctorSociety from '../society/createDoctorSociety';
import getSocietyByUserId from '../society/getSocietyByUserId';
import createUser from '../user/createUser';
import puaTemplate from '../user/societyTemplateForUserInvitation/pua'
import palesTemplate from '../user/societyTemplateForUserInvitation/pales'
import bcrypt from 'bcryptjs';
import createLog from '../activityLogs/insertUserLogs';
import logType from '../activityLogs/LogTypes';
import moment from 'moment'
import R from 'ramda'
import {FOREIGN_NONMEMBER} from '../items/PaymentStatusConstant'

const requestParameters = [
    {
        name: 'body',
        required: true,
        in: 'body',
        schema: {
            type: 'object',
            properties: {
                email: {
                    type: 'string',
                }
            },
        },
    },
];

const responseSchema = {};
const spec = {
    parameters: requestParameters,
    responses: {
        '201': {
            description: 'User Invited',
            schema: responseSchema,
        },
        '301': {
            description: 'User is Already Existing',
            schema: responseSchema,
        }
    },
};

const PALES = 'PALES'
const PUA = 'PUA'

const mailTemplate = {
    [PALES]: palesTemplate,
    [PUA]: puaTemplate,
}

const handler = ({request}) => {
    const {
        email,
        membershipStatus,
        society,
        societyCode,
        name,
        operatorId,
        operator,
    } = request.body;
    const countryCode = request.headers['cf-ipcountry']

    const currentMembership = countryCode === 'PH' ? membershipStatus : FOREIGN_NONMEMBER
    const salt = bcrypt.genSaltSync(10);
    const inviteFormat = {
        email: email,
        type: 'doctor',
        membershipStatus: currentMembership,
        active: false,
        firstName: name,
        dateCreated: new Date(),
        registrationKey: salt,
        countryCode,
    };

    return getUserByEmail(email)
    .then((user) => {
        if(R.isNil(user)) {
            return createUser(inviteFormat);
        }

        return user
    })
    .then((user) => getSocietyByUserId(user._id, societyCode, user))
    .then((userSociety) => {
        if(R.not(userSociety) || !userSociety.active) {
            const userId = userSociety.userId
            const date = moment().format('MMM DD YYYY m:s A');

            createLog({
                type: 'INVITED',
                triggeredBy: operatorId,
                information: {
                    userId,
                    societyCode,
                    message: `${date}: invited ${name} (${email}) as ${membershipStatus}`,
                },
            })

            const societyTemplate = mailTemplate[societyCode]

            const emailHtml = societyTemplate({
                email,
                name,
                registrationKey: userSociety.registrationKey,
            })

            mailgun(emailHtml, email);

            createDoctorSociety(userId, {
                _id: society,
                status: membershipStatus,
                active: true,
                societyCode: societyCode,
            });

            return formatResult(201, 'Invitation completed');
        }

        return formatResult(301, 'user is existing');
    })
    .catch((error) => {
        throw Error(error)
    })
};

export const post = createEndPoint(spec, handler);
