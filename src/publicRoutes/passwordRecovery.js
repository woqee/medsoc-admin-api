/* @flow */
import passwordRecovery from '../user/passwordRecovery';
import getWebUrl from '../getWebUrlByEnvironment'
import mailgun from '../mailgun'
import bcrypt from 'bcryptjs';
import shortid from 'shortid'

export default (req, res) => {
    const {email} = req.query
    const passwordRecoveryToken = shortid.generate();
    const link = `${getWebUrl('/')}password-reset/${passwordRecoveryToken}`

    const emailData = {
        from: 'Med-Societies Support <support@med-societies.com',
        to: email,
        subject: 'Reset your password',
        html: `
            <br/>
            <br/>
            You can reset your password in Med Societies App by clicking the
            link below
            <br/>
            <br/>
            <a href="${link}">${link}</a>
            <br/>
            <br/>
        `
    }
    passwordRecovery(email, passwordRecoveryToken)
    .then(() => {
        mailgun(emailData, email)

        res.status(201).json({message: 'email was sent to you'})
    })
    .catch(() => {
        res.status(500).json({message: 'password reset failed'})
    })

}
