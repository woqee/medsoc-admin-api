/* @flow */
import createEndPoint, {formatResult} from '../../createEndpoint';

const requestParameters = [
    {
        in: 'query',
        name: 'key',
        type: 'string',
        required: true,
    }
];

const responseSchema = {
    type: 'object',
    items: {
        type: 'object',
        properties: {
            societies: {
                type: 'object',
            },
        },
    },
};

const spec = {
    parameters: requestParameters,
    responses: {
        '200': {
            description: 'Successful request',
            schema: responseSchema,
        },
    },
};

const handler = ({request}) => {
    const {key} = request.query;

    console.log(key);
};

export const get = createEndPoint(spec, handler);
