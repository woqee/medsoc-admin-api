/* @flow */
import resetPassword from '../user/resetPassword';
import getWebUrl from '../getWebUrlByEnvironment'
import mailgun from '../mailgun'

export default (req, res) => {
    return resetPassword(req.body)
    .then(() => {
        res.status(201).json({message: 'password generated'})
    })
    .catch((error) => {
        res.status(500).json({message: error.message})
    })
}
