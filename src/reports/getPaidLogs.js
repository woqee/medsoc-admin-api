/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';

export default (societyCode) => {
    const aggregation = [
         {
             $match: {
               'item.societyCode': societyCode,
               'chargeInfo.risk': 'approved',
             },
         },
         {
            $lookup:{
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "payee",
            },
         },
         {
             $unwind: '$payee'
         },
    ];

    return mongoConnect.then((db) =>
        db.collection(COLLECTION.payments).aggregate(aggregation).toArray()
    )
}
