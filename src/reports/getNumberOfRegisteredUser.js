/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import LogTypes from '../activityLogs/LogTypes'

export default () => {
    return mongoConnect.then((db) =>
        db.collection(COLLECTION.users).count({
            active: true,
        })
    )
}
