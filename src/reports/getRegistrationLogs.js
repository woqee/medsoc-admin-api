/* @flow */

import mongoConnect from '../mongodb/mongoConnect';
import {COLLECTION} from '../mongodb/mongoConstant';
import LogTypes from '../activityLogs/LogTypes'

export default (societyCode) => {
    console.log(LogTypes.ADDED_MEMBER)
    const aggregation = [
         {
             $match: {
               'information.societyCode': societyCode,
               type: LogTypes.ADDED_MEMBER,
             },
         },
         {
             $project: {
                 _id: 1,
                 date: '$logDate',
                 message: '$information.message',
             }
         }
    ];

    return mongoConnect.then((db) =>
        db.collection(COLLECTION.logs).aggregate(aggregation).toArray()
    )
}
